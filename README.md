# Atlas Swap Interface

## Project setup
```
apt install docker docker.io containerd docker-compose git
```

### Run development server
```
sudo docker-compose -f docker-compose.yml up
```

Open browser http://127.0.0.1:3000/ 

import React from 'react'
import styled from 'styled-components'
import { SwapPoolTabs } from '../../components/NavigationTabs'
import { RowBetween } from '../../components/Row'
import { AutoColumn } from '../../components/Column'
import { CardSection, DataCard } from '../../components/earn/styled'
import { TYPE } from '../../theme'
import ImageLogo from '../../assets/images/docs/Atlas-Logo.png'
import ImageLp from '../../assets/images/docs/scheme.png'

const PageWrapper = styled(AutoColumn)`
  max-width: 850px;
  width: 100%;
`

const VoteCard = styled(DataCard)`
  overflow: hidden;
  background: radial-gradient(76.02% 75.41% at 1.84% 0%, rgb(88 82 85 / 50%) 0%, rgb(44 61 86 / 50%) 100%);
`

const ImageStyle = styled.img`
  max-width: 100%;
`

export default function Docs() {
  return (
    <>
      <PageWrapper>
        <SwapPoolTabs active={'pool'} />
        <VoteCard>
          <CardSection>
            <AutoColumn gap="md">
              <RowBetween>
                <TYPE.white fontSize={14}>
                  <h1>Liquidity yield pools</h1>

                  <ImageStyle src={ImageLogo} alt="Atlas Dex Swap" />

                  <h2>EXORDIUM</h2>
                  <p>
                    Each liquidity pool of the Atlas DEX Swap is a trading platform for ERC20 tokens pairs. The balance
                    of the token is equaled to zero when a pool contract is created and for facilitating trades of the
                    pool, someone must seed it with an initial deposit of each token.
                  </p>
                  <p>
                    The initial price of the pool is set by the first liquidity provider. The liquidity providers are
                    being incentivized to deposit an equal value of both tokens into the pool.
                  </p>
                  <p>
                    And here is the reason:
                    <br />
                    In case if the first liquidity provider deposits tokens at a ratio different from the current market
                    rate then this creates arbitrage possibilities for others, which is likely to be taken by an
                    external party.
                  </p>

                  <p>
                    So, when other providers of the liquidity are adding to an existing pool, they should deposit the
                    liquidity to pair tokens proportional to the current price. If not, then the added liquidity can be
                    arbitraged as well. But in case if other providers of the liquidity see that the current price isn't
                    correct, they can arbitrage it to the level they want and add liquidity at that price.
                  </p>

                  <h2>ATLAS TOKENS OF LIQUIDITY</h2>

                  <p>
                    Whenever liquidity is deposited into a pool, ATL tokens known as Atlas Tokens of Liquidity are
                    minted and sent to the liquidity provider’s ERC20 address. These ATL tokens represent a given
                    liquidity provider’s contribution to a pool. The proportion of the pool’s liquidity provided
                    determines the number of Atlas Tokens of Liquidity (ATL) the provider receives. If the provider is
                    minting a new pool, the number of ATL tokens they will receive will equal √(A x B), where A and B
                    represent the amount of each token provided.
                  </p>

                  <p>
                    Whenever any trade occurs, a 0.3% fee is charged to the transaction sender. This fee is distributed
                    proportionally to all liquidity providers in the pool upon completion of the trade.
                  </p>

                  <p>
                    To get the liquidity back and any fees accrued, the liquidity providers must “burn” their ATL
                    tokens, effectively exchanging them for their portion of the liquidity pool, plus the proportional
                    fee allocation.
                  </p>

                  <p>
                    As ATL tokens are themselves tradable assets, the liquidity providers can sell, transfer, or
                    otherwise use their ATL tokens in any way they see fit.
                  </p>

                  <ImageStyle src={ImageLp} alt="LP" />

                  <h2>LIQUIDITY POOLS</h2>

                  <p>
                    Atlas DEX Swap doesn’t use the order book to derive the price of any asset or to match buyers and
                    sellers of the tokens. Instead of this Atlas DEX Swap uses the Liquidity Pools.
                  </p>

                  <p>
                    Liquidity is typically represented by discrete orders placed by users onto a centrally operated
                    order book. A participant looking to provide liquidity or make markets must actively manage their
                    orders, continuously updating them in response to the activity of others in the marketplace.
                  </p>
                  <p>
                    While order books are foundational to finance and work great in certain situations, but they suffer
                    from a few important limitations of decentralized or blockchain-native settings. Order books require
                    intermediary infrastructure to host the order book and match orders. This creates points of control
                    and adds additional layers of complexity. They also require active participation and management from
                    market makers (MM) who usually use algorithms and complicated infrastructure, limiting participation
                    to advanced traders. Order books were invented in a world with relatively few assets being traded,
                    so it isn't surprising they aren’t ideal for the ecosystem where anyone can create their own token
                    and those tokens usually have low liquidity. In sum, with the infrastructural trade-offs presented
                    by a platform like Ethereum, order books aren't the native architecture for implementing a liquidity
                    protocol on a blockchain.
                  </p>
                  <p>
                    Atlas DEX Swap focuses on the advantages of Ethereum to rethink token swaps from the first
                    principles. A liquidity protocol should take advantage of the trusted code execution environment,
                    the autonomous and perpetually running virtual machine, and an open, permissionless, and inclusive
                    access model that produces an exponentially growing ecosystem of virtual assets.
                  </p>
                  <p>
                    It's vital to remember that a Pool is nothing more than a smart contract that users can control by
                    calling functions on it. On a Pool contract instance, swapping tokens is called swap, while
                    supplying liquidity is called a deposit.
                  </p>
                  <p>
                    Developers can interact directly with the smart contracts and integrate Atlas DEX Swap functionality
                    into their own applications without relying on intermediaries or requiring permission, similar to
                    how end-users interact with the Atlas DEX Swap protocol through the Interface (which in turn
                    interacts with the underlying contracts).
                  </p>
                </TYPE.white>
              </RowBetween>
            </AutoColumn>
          </CardSection>
        </VoteCard>
      </PageWrapper>
    </>
  )
}

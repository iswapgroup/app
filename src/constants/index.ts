//import { ChainId, JSBI, Percent, Token, WETH } from '@uniswap/sdk'
import { ChainId, JSBI, Percent, Token, WETH } from '../lib/'
import { AbstractConnector } from '@web3-react/abstract-connector'
import EthIcon from '../assets/images/networks/eth.webp'
import BscIcon from '../assets/images/networks/bsc.webp'
import GnosysIcon from '../assets/images/networks/gnosis.webp'
import PolygonIcon from '../assets/images/networks/polygon.webp'
import FantomIcon from '../assets/images/networks/fantom.jpg'
import MoonriverIcon from '../assets/images/networks/moonriver.webp'
import ArbitrumIcon from '../assets/images/networks/arbitrum.webp'
import AvalanceIcon from '../assets/images/networks/avalanche.webp'
import HarmonyIcon from '../assets/images/networks/harmonyone.jpg'
import TelosIcon from '../assets/images/networks/telos.webp'
import FuseIcon from '../assets/images/networks/fuse.webp'
import HecoIcon from '../assets/images/networks/heco.jpg'
import OkexIcon from '../assets/images/networks/okex.webp'

import { fortmatic, injected, portis, walletconnect, walletlink } from '../connectors'

export const NATIVE = {
  1: {
    symbol: 'ETH',
    name: 'Ether',
    icon: EthIcon,
    scan: {
      name: 'Etherscan',
      url: 'https://etherscan.com'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  40: {
    symbol: 'TLOS',
    name: 'Telos',
    icon: TelosIcon,
    scan: {
      name: 'Telos Explorer',
      url: 'https://mainnet.telos.net/evm'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  56: {
    symbol: 'BNB',
    name: 'Binance chain',
    icon: BscIcon,
    scan: {
      name: 'BScan',
      url: 'https://bscscan.com'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.25
    }
  },
  66: {
    symbol: 'OKT',
    name: 'OKExChain',
    icon: OkexIcon,
    scan: {
      name: 'OKLink',
      url: 'https://www.oklink.com/en/oec'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  100: {
    symbol: 'XDAI',
    name: 'xDai',
    icon: GnosysIcon,
    scan: {
      name: 'Blockscout',
      url: 'https://blockscout.com/poa/xdai'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.9
    }
  },
  122: {
    symbol: 'FUSE',
    name: 'Fuse',
    icon: FuseIcon,
    scan: {
      name: 'Fuse explorer',
      url: 'https://explorer.fuse.io'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.6
    }
  },
  128: {
    symbol: 'HT',
    name: 'Huobi Token',
    icon: HecoIcon,
    scan: {
      name: 'Heco Info',
      url: 'https://hecoinfo.com'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  137: {
    symbol: 'MATIC',
    name: 'Matic',
    icon: PolygonIcon,
    scan: {
      name: 'Polygon Scan',
      url: 'https://polygonscan.com'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  250: {
    symbol: 'FTM',
    name: 'Fantom',
    icon: FantomIcon,
    scan: {
      name: 'FTMscan',
      url: 'https://ftmscan.com'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  1285: {
    symbol: 'MOVR',
    name: 'Moonriver',
    icon: MoonriverIcon,
    scan: {
      name: 'Moonscan',
      url: 'https://moonriver.moonscan.io'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  42161: {
    symbol: 'ETH',
    name: 'Ether',
    icon: ArbitrumIcon,
    scan: {
      name: 'Arbiscan',
      url: 'https://arbiscan.io'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  43114: {
    symbol: 'AVAX',
    name: 'Avalanche',
    icon: AvalanceIcon,
    scan: {
      name: 'Snowtrace',
      url: 'https://snowtrace.io'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  },
  1666600000: {
    symbol: 'ONE',
    name: 'Harmony',
    icon: HarmonyIcon,
    scan: {
      name: 'Harmony explorer',
      url: 'https://explorer.harmony.one'
    },
    fee: {
      iswap: 0.3,
      uniswap: 0.3
    }
  }
}

const FACTORIES = {
  1: process.env.REACT_APP_ATLAS_ETH_FACTORY,
  40: process.env.REACT_APP_ATLAS_TELOS_FACTORY,
  56: process.env.REACT_APP_ATLAS_BSC_FACTORY,
  66: process.env.REACT_APP_ATLAS_OKEX_FACTORY,
  100: process.env.REACT_APP_ATLAS_XDAI_FACTORY,
  122: process.env.REACT_APP_ATLAS_FUSE_FACTORY,
  128: process.env.REACT_APP_ATLAS_HECO_FACTORY,
  137: process.env.REACT_APP_ATLAS_MATIC_FACTORY,
  250: process.env.REACT_APP_ATLAS_FANTOM_FACTORY,
  1285: process.env.REACT_APP_ATLAS_MOONRIVER_FACTORY,
  42161: process.env.REACT_APP_ATLAS_ARBITRUM_FACTORY,
  43114: process.env.REACT_APP_ATLAS_AVALANCHE_FACTORY,
  1666600000: process.env.REACT_APP_ATLAS_HARMONY_FACTORY
}

const ROUTERS = {
  1: process.env.REACT_APP_ATLAS_ETH_ROUTER,
  40: process.env.REACT_APP_ATLAS_TELOS_ROUTER,
  56: process.env.REACT_APP_ATLAS_BSC_ROUTER,
  66: process.env.REACT_APP_ATLAS_OKEX_ROUTER,
  100: process.env.REACT_APP_ATLAS_XDAI_ROUTER,
  122: process.env.REACT_APP_ATLAS_FUSE_ROUTER,
  128: process.env.REACT_APP_ATLAS_HECO_ROUTER,
  137: process.env.REACT_APP_ATLAS_MATIC_ROUTER,
  250: process.env.REACT_APP_ATLAS_FANTOM_ROUTER,
  1285: process.env.REACT_APP_ATLAS_MOONRIVER_ROUTER,
  42161: process.env.REACT_APP_ATLAS_ARBITRUM_ROUTER,
  43114: process.env.REACT_APP_ATLAS_AVALANCHE_ROUTER,
  1666600000: process.env.REACT_APP_ATLAS_HARMONY_ROUTER
}

const INIT_CODE_HASHES = {
  1: process.env.REACT_APP_ATLAS_ETH_INIT_CODE_HASH,
  40: process.env.REACT_APP_ATLAS_TELOS_INIT_CODE_HASH,
  56: process.env.REACT_APP_ATLAS_BSC_INIT_CODE_HASH,
  66: process.env.REACT_APP_ATLAS_OKEX_INIT_CODE_HASH,
  100: process.env.REACT_APP_ATLAS_XDAI_INIT_CODE_HASH,
  122: process.env.REACT_APP_ATLAS_FUSE_INIT_CODE_HASH,
  128: process.env.REACT_APP_ATLAS_HECO_INIT_CODE_HASH,
  137: process.env.REACT_APP_ATLAS_MATIC_INIT_CODE_HASH,
  250: process.env.REACT_APP_ATLAS_FANTOM_INIT_CODE_HASH,
  1285: process.env.REACT_APP_ATLAS_MOONRIVER_INIT_CODE_HASH,
  42161: process.env.REACT_APP_ATLAS_ARBITRUM_INIT_CODE_HASH,
  43114: process.env.REACT_APP_ATLAS_AVALANCHE_INIT_CODE_HASH,
  1666600000: process.env.REACT_APP_ATLAS_HARMONY_INIT_CODE_HASH
}

const FACTORIES_UNISWAP = {
  1: process.env.REACT_APP_UNISWAP_FACTORY,
  40: process.env.REACT_APP_SUSHISWAP_TELOS_FACTORY,
  56: process.env.REACT_APP_PANCKAKE_FACTORY,
  66: process.env.REACT_APP_SUSHISWAP_OKEX_FACTORY,
  100: process.env.REACT_APP_SUSHISWAP_XDAI_FACTORY,
  122: process.env.REACT_APP_SUSHISWAP_FUSE_FACTORY,
  128: process.env.REACT_APP_SUSHISWAP_HECO_FACTORY,
  137: process.env.REACT_APP_QUICKSWAP_FACTORY,
  250: process.env.REACT_APP_SUSHISWAP_FANTOM_FACTORY,
  1285: process.env.REACT_APP_SUSHISWAP_MOONRIVER_FACTORY,
  42161: process.env.REACT_APP_SUSHISWAP_ARBITRUM_FACTORY,
  43114: process.env.REACT_APP_SUSHISWAP_AVALANCHE_FACTORY,
  1666600000: process.env.REACT_APP_SUSHISWAP_HARMONY_FACTORY
}

const ROUTERS_UNISWAP = {
  1: process.env.REACT_APP_UNISWAP_ROUTER,
  40: process.env.REACT_APP_SUSHISWAP_TELOS_ROUTER,
  56: process.env.REACT_APP_PANCKAKE_ROUTER,
  66: process.env.REACT_APP_SUSHISWAP_OKEX_ROUTER,
  100: process.env.REACT_APP_SUSHISWAP_XDAI_ROUTER,
  122: process.env.REACT_APP_SUSHISWAP_FUSE_ROUTER,
  128: process.env.REACT_APP_SUSHISWAP_HECO_ROUTER,
  137: process.env.REACT_APP_QUICKSWAP_ROUTER,
  250: process.env.REACT_APP_SUSHISWAP_FANTOM_ROUTER,
  1285: process.env.REACT_APP_SUSHISWAP_MOONRIVER_ROUTER,
  42161: process.env.REACT_APP_SUSHISWAP_ARBITRUM_ROUTER,
  43114: process.env.REACT_APP_SUSHISWAP_AVALANCHE_ROUTER,
  1666600000: process.env.REACT_APP_SUSHISWAP_HARMONY_ROUTER
}

const INIT_CODE_HASHES_UNISWAP = {
  1: process.env.REACT_APP_UNISWAP_INIT_CODE_HASH,
  40: process.env.REACT_APP_SUSHISWAP_TELOS_INIT_CODE_HASH,
  56: process.env.REACT_APP_PANCKAKE_INIT_CODE_HASH,
  66: process.env.REACT_APP_SUSHISWAP_OKEX_INIT_CODE_HASH,
  100: process.env.REACT_APP_SUSHISWAP_XDAI_INIT_CODE_HASH,
  122: process.env.REACT_APP_SUSHISWAP_FUSE_INIT_CODE_HASH,
  128: process.env.REACT_APP_SUSHISWAP_HECO_INIT_CODE_HASH,
  137: process.env.REACT_APP_QUICKSWAP_INIT_CODE_HASH,
  250: process.env.REACT_APP_SUSHISWAP_FANTOM_INIT_CODE_HASH,
  1285: process.env.REACT_APP_SUSHISWAP_MOONRIVER_INIT_CODE_HASH,
  42161: process.env.REACT_APP_SUSHISWAP_ARBITRUM_INIT_CODE_HASH,
  43114: process.env.REACT_APP_SUSHISWAP_AVALANCHE_INIT_CODE_HASH,
  1666600000: process.env.REACT_APP_SUSHISWAP_HARMONY_INIT_CODE_HASH
}

export { FACTORIES, ROUTERS, INIT_CODE_HASHES, FACTORIES_UNISWAP, ROUTERS_UNISWAP, INIT_CODE_HASHES_UNISWAP }

export const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

export { PRELOADED_PROPOSALS } from './proposals'

// a list of tokens by chain
type ChainTokenList = {
  [chainId in ChainId]: Token[]
}

export const AMPL = new Token(ChainId.MAINNET, '0xD46bA6D942050d489DBd938a2C909A5d5039A161', 9, 'AMPL', 'Ampleforth')
export const DAI = new Token(ChainId.MAINNET, '0x6B175474E89094C44Da98b954EedeAC495271d0F', 18, 'DAI', 'Dai Stablecoin')
export const USDC = new Token(ChainId.MAINNET, '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48', 6, 'USDC', 'USD//C')
export const USDT = new Token(ChainId.MAINNET, '0xdAC17F958D2ee523a2206206994597C13D831ec7', 6, 'USDT', 'Tether USD')
export const WBTC = new Token(ChainId.MAINNET, '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599', 8, 'WBTC', 'Wrapped BTC')
export const FEI = new Token(ChainId.MAINNET, '0x956F47F50A910163D8BF957Cf5846D573E7f87CA', 18, 'FEI', 'Fei USD')
export const TRIBE = new Token(ChainId.MAINNET, '0xc7283b66Eb1EB5FB86327f08e1B5816b0720212B', 18, 'TRIBE', 'Tribe')
export const FRAX = new Token(ChainId.MAINNET, '0x853d955aCEf822Db058eb8505911ED77F175b99e', 18, 'FRAX', 'Frax')
export const FXS = new Token(ChainId.MAINNET, '0x3432B6A60D23Ca0dFCa7761B7ab56459D9C964D0', 18, 'FXS', 'Frax Share')
export const renBTC = new Token(ChainId.MAINNET, '0xEB4C2781e4ebA804CE9a9803C67d0893436bB27D', 8, 'renBTC', 'renBTC')

//polygon
const DAI_POLYGON = new Token(ChainId.MATIC, '0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063', 18, 'DAI', 'Dai Stablecoin')
const WETH_POLYGON = new Token(ChainId.MATIC, '0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619', 18, 'WETH', 'Ethereum')
const USDC_POLYGON = new Token(ChainId.MATIC, '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174', 6, 'USDC', 'USD Coin')
const USDT_POLYGON = new Token(ChainId.MATIC, '0xc2132D05D31c914a87C6611C10748AEb04B58e8F', 6, 'USDT', 'Tether USD')
const WBTC_POLYGON = new Token(ChainId.MATIC, '0x1BFD67037B42Cf73acF2047067bd4F2C47D9BfD6', 8, 'WBTC', 'WBTC')

//arbitrum
const DAI_ARBITRUM = new Token(
  ChainId.ARBITRUM,
  '0xDA10009cBd5D07dd0CeCc66161FC93D7c9000da1',
  18,
  'DAI',
  'Dai Stablecoin'
)
const WETH_ARBITRUM = new Token(ChainId.ARBITRUM, '0x82aF49447D8a07e3bd95BD0d56f35241523fBab1', 18, 'WETH', 'Ethereum')
const USDC_ARBITRUM = new Token(ChainId.ARBITRUM, '0xFF970A61A04b1cA14834A43f5dE4533eBDDB5CC8', 6, 'USDC', 'USD Coin')
const USDT_ARBITRUM = new Token(ChainId.ARBITRUM, '0xFd086bC7CD5C481DCC9C85ebE478A1C0b69FCbb9', 6, 'USDT', 'Tether USD')
const WBTC_ARBITRUM = new Token(ChainId.ARBITRUM, '0x2f2a2543B76A4166549F7aaB2e75Bef0aefC5B0f', 8, 'WBTC', 'WBTC')

//avalanche
const DAI_AVALANCHE = new Token(
  ChainId.AVALANCHE,
  '0xd586E7F844cEa2F87f50152665BCbc2C279D8d70',
  18,
  'DAI',
  'Dai Stablecoin'
)
const WETH_AVALANCHE = new Token(
  ChainId.AVALANCHE,
  '0x49D5c2BdFfac6CE2BFdB6640F4F80f226bc10bAB',
  18,
  'WETH',
  'Ethereum'
)
const USDC_AVALANCHE = new Token(ChainId.AVALANCHE, '0xA7D7079b0FEaD91F3e65f86E8915Cb59c1a4C664', 6, 'USDC', 'USD Coin')
const USDT_AVALANCHE = new Token(
  ChainId.AVALANCHE,
  '0xc7198437980c041c805A1EDcbA50c1Ce5db95118',
  6,
  'USDT',
  'Tether USD'
)
const WBTC_AVALANCHE = new Token(ChainId.AVALANCHE, '0x50b7545627a5162F82A992c33b87aDc75187B218', 8, 'WBTC', 'WBTC')

//moonriver
const DAI_MOONRIVER = new Token(
  ChainId.MOONRIVER,
  '0x80A16016cC4A2E6a2CACA8a4a498b1699fF0f844',
  18,
  'DAI',
  'Dai Stablecoin'
)
const WETH_MOONRIVER = new Token(
  ChainId.MOONRIVER,
  '0x639A647fbe20b6c8ac19E48E2de44ea792c62c5C',
  18,
  'WETH',
  'Ethereum'
)
const USDC_MOONRIVER = new Token(ChainId.MOONRIVER, '0xE3F5a90F9cb311505cd691a46596599aA1A0AD7D', 6, 'USDC', 'USD Coin')
const USDT_MOONRIVER = new Token(
  ChainId.MOONRIVER,
  '0xB44a9B6905aF7c801311e8F4E76932ee959c663C',
  6,
  'USDT',
  'Tether USD'
)
const WBTC_MOONRIVER = new Token(ChainId.MOONRIVER, '0xE6a991Ffa8CfE62B0bf6BF72959A3d4f11B2E0f5', 8, 'WBTC', 'WBTC')

//fantom
const DAI_FANTOM = new Token(ChainId.FANTOM, '0x8D11eC38a3EB5E956B052f67Da8Bdc9bef8Abf3E', 18, 'DAI', 'Dai Stablecoin')
const WETH_FANTOM = new Token(ChainId.FANTOM, '0x74b23882a30290451A17c44f4F05243b6b58C76d', 18, 'WETH', 'Ethereum')
const USDC_FANTOM = new Token(ChainId.FANTOM, '0x04068DA6C83AFCFA0e13ba15A6696662335D5B75', 6, 'USDC', 'USD Coin')
const USDT_FANTOM = new Token(ChainId.FANTOM, '0x049d68029688eAbF473097a2fC38ef61633A3C7A', 6, 'USDT', 'Tether USD')
const WBTC_FANTOM = new Token(ChainId.FANTOM, '0x321162Cd933E2Be498Cd2267a90534A804051b11', 8, 'WBTC', 'WBTC')

//bsc
const DAI_BSC = new Token(ChainId.BSC, '0x1AF3F329e8BE154074D8769D1FFa4eE058B1DBc3', 18, 'DAI', 'Dai Stablecoin')
const USDC_BSC = new Token(ChainId.BSC, '0x8AC76a51cc950d9822D68b83fE1Ad97B32Cd580d', 6, 'USDC', 'USD Coin')
const USDT_BSC = new Token(ChainId.BSC, '0x55d398326f99059fF775485246999027B3197955', 6, 'USDT', 'Tether USD')

//xdai
const WETH_XDAI = new Token(ChainId.XDAI, '0x6A023CCd1ff6F2045C3309768eAd9E68F978f6e1', 18, 'WETH', 'Ethereum')
const USDC_XDAI = new Token(ChainId.XDAI, '0xDDAfbb505ad214D7b80b1f830fcCc89B60fb7A83', 6, 'USDC', 'USD Coin')
const USDT_XDAI = new Token(ChainId.XDAI, '0x4ECaBa5870353805a9F068101A40E0f32ed605C6', 6, 'USDT', 'Tether USD')
const WBTC_XDAI = new Token(ChainId.XDAI, '0x8e5bBbb09Ed1ebdE8674Cda39A0c169401db4252', 8, 'WBTC', 'WBTC')

//harmony
const DAI_HARMONY = new Token(
  ChainId.HARMONY,
  '0xEf977d2f931C1978Db5F6747666fa1eACB0d0339',
  18,
  'DAI',
  'Dai Stablecoin'
)
const WETH_HARMONY = new Token(ChainId.HARMONY, '0x6983d1e6def3690c4d616b13597a09e6193ea013', 6, 'USDC', 'USD Coin')
const USDC_HARMONY = new Token(ChainId.HARMONY, '0x985458E523dB3d53125813eD68c274899e9DfAb4', 6, 'USDC', 'USD Coin')
const USDT_HARMONY = new Token(ChainId.HARMONY, '0x3C2B8Be99c50593081EAA2A724F0B8285F5aba8f', 6, 'USDT', 'Tether USD')

//telos evm
const USDC_TELOS = new Token(ChainId.TELOS, '0x818ec0A7Fe18Ff94269904fCED6AE3DaE6d6dC0b', 6, 'USDC', 'USD Coin')
const USDT_TELOS = new Token(ChainId.TELOS, '0xeFAeeE334F0Fd1712f9a8cc375f427D9Cdd40d73', 6, 'USDT', 'Tether USD')

//fantom
const DAI_FUSE = new Token(ChainId.FUSE, '0x94Ba7A27c7A95863d1bdC7645AC2951E0cca06bA', 18, 'DAI', 'Dai Stablecoin')
const WETH_FUSE = new Token(ChainId.FUSE, '0xa722c13135930332Eb3d749B2F0906559D2C5b99', 18, 'WETH', 'Ethereum')
const USDC_FUSE = new Token(ChainId.FUSE, '0x620fd5fa44BE6af63715Ef4E65DDFA0387aD13F5', 6, 'USDC', 'USD Coin')
const USDT_FUSE = new Token(ChainId.FUSE, '0xFaDbBF8Ce7D5b7041bE672561bbA99f79c532e10', 6, 'USDT', 'Tether USD')
const WBTC_FUSE = new Token(ChainId.FUSE, '0x33284f95ccb7B948d9D352e1439561CF83d8d00d', 8, 'WBTC', 'WBTC')

//fantom
const DAI_OKEX = new Token(ChainId.OKEX, '0x21cDE7E32a6CAF4742d00d44B07279e7596d26B9', 18, 'DAI', 'Dai Stablecoin')
const USDC_OKEX = new Token(ChainId.OKEX, '0xc946DAf81b08146B1C7A8Da2A851Ddf2B3EAaf85', 6, 'USDC', 'USD Coin')
const USDT_OKEX = new Token(ChainId.OKEX, '0x382bB369d343125BfB2117af9c149795C6C65C50', 6, 'USDT', 'Tether USD')
const WBTC_OKEX = new Token(ChainId.OKEX, '0x506f731F7656e2FB34b587B912808f2a7aB640BD', 8, 'WBTC', 'WBTC')

// Block time here is slightly higher (~1s) than average in order to avoid ongoing proposals past the displayed time
export const AVERAGE_BLOCK_TIME_IN_SECS = 13
export const PROPOSAL_LENGTH_IN_BLOCKS = 40_320
export const PROPOSAL_LENGTH_IN_SECS = AVERAGE_BLOCK_TIME_IN_SECS * PROPOSAL_LENGTH_IN_BLOCKS

export const GOVERNANCE_ADDRESS = '0x5e4be8Bc9637f0EAA1A755019e06A68ce081D58F'

export const TIMELOCK_ADDRESS = '0x1a9C8182C09F50C8318d769245beA52c32BE35BC'

const UNI_ADDRESS = '0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984'
export const UNI: { [chainId in ChainId]?: Token } = {
  [ChainId.MAINNET]: new Token(ChainId.MAINNET, UNI_ADDRESS, 18, 'UNI', 'Uniswap'),
  [ChainId.RINKEBY]: new Token(ChainId.RINKEBY, UNI_ADDRESS, 18, 'UNI', 'Uniswap'),
  [ChainId.ROPSTEN]: new Token(ChainId.ROPSTEN, UNI_ADDRESS, 18, 'UNI', 'Uniswap'),
  [ChainId.GÖRLI]: new Token(ChainId.GÖRLI, UNI_ADDRESS, 18, 'UNI', 'Uniswap'),
  [ChainId.KOVAN]: new Token(ChainId.KOVAN, UNI_ADDRESS, 18, 'UNI', 'Uniswap'),
  [ChainId.BSC]: new Token(ChainId.BSC, '0xbf5140a22578168fd562dccf235e5d43a02ce9b1', 18, 'UNI', 'Uniswap'),
  [ChainId.MATIC]: new Token(ChainId.MATIC, '0xb33EaAd8d922B1083446DC23f610c2567fB5180f', 18, 'UNI', 'Uniswap'),
  [ChainId.ARBITRUM]: new Token(ChainId.ARBITRUM, '0xfa7f8980b0f1e64a2062791cc3b0871572f1f7f0', 18, 'UNI', 'Uniswap'),
  [ChainId.AVALANCHE]: new Token(ChainId.AVALANCHE, '0x8ebaf22b6f053dffeaf46f4dd9efa95d89ba8580', 18, 'UNI', 'Uniswap')
}

export const COMMON_CONTRACT_NAMES: { [address: string]: string } = {
  [UNI_ADDRESS]: 'UNI',
  [GOVERNANCE_ADDRESS]: 'Governance',
  [TIMELOCK_ADDRESS]: 'Timelock'
}

// TODO: specify merkle distributor for mainnet
export const MERKLE_DISTRIBUTOR_ADDRESS: { [chainId in ChainId]?: string } = {
  [ChainId.MAINNET]: '0x090D4613473dEE047c3f2706764f49E0821D256e'
}

export const WETH_ONLY: ChainTokenList = {
  [ChainId.MAINNET]: [WETH[ChainId.MAINNET]],
  [ChainId.ROPSTEN]: [WETH[ChainId.ROPSTEN]],
  [ChainId.RINKEBY]: [WETH[ChainId.RINKEBY]],
  [ChainId.GÖRLI]: [WETH[ChainId.GÖRLI]],
  [ChainId.TELOS]: [WETH[ChainId.TELOS]],
  [ChainId.KOVAN]: [WETH[ChainId.KOVAN]],
  [ChainId.BSC]: [WETH[ChainId.BSC]],
  [ChainId.OKEX]: [WETH[ChainId.OKEX]],
  [ChainId.XDAI]: [WETH[ChainId.XDAI]],
  [ChainId.FUSE]: [WETH[ChainId.FUSE]],
  [ChainId.HECO]: [WETH[ChainId.HECO]],
  [ChainId.MATIC]: [WETH[ChainId.MATIC]],
  [ChainId.FANTOM]: [WETH[ChainId.FANTOM]],
  [ChainId.MOONRIVER]: [WETH[ChainId.MOONRIVER]],
  [ChainId.ARBITRUM]: [WETH[ChainId.ARBITRUM]],
  [ChainId.AVALANCHE]: [WETH[ChainId.AVALANCHE]],
  [ChainId.HARMONY]: [WETH[ChainId.HARMONY]]
}

// used to construct intermediary pairs for trading
export const BASES_TO_CHECK_TRADES_AGAINST: ChainTokenList = {
  ...WETH_ONLY,
  [ChainId.MAINNET]: [...WETH_ONLY[ChainId.MAINNET], DAI, USDC, USDT, WBTC],
  [ChainId.MATIC]: [...WETH_ONLY[ChainId.MATIC], DAI_POLYGON, WETH_POLYGON, USDC_POLYGON, USDT_POLYGON, WBTC_POLYGON],
  [ChainId.ARBITRUM]: [
    ...WETH_ONLY[ChainId.ARBITRUM],
    DAI_ARBITRUM,
    WETH_ARBITRUM,
    USDC_ARBITRUM,
    USDT_ARBITRUM,
    WBTC_ARBITRUM
  ],
  [ChainId.AVALANCHE]: [
    ...WETH_ONLY[ChainId.AVALANCHE],
    DAI_AVALANCHE,
    WETH_AVALANCHE,
    USDC_AVALANCHE,
    USDT_AVALANCHE,
    WBTC_AVALANCHE
  ],
  [ChainId.MOONRIVER]: [
    ...WETH_ONLY[ChainId.MOONRIVER],
    DAI_MOONRIVER,
    WETH_MOONRIVER,
    USDC_MOONRIVER,
    USDT_MOONRIVER,
    WBTC_MOONRIVER
  ],
  [ChainId.FANTOM]: [...WETH_ONLY[ChainId.FANTOM], DAI_FANTOM, WETH_FANTOM, USDC_FANTOM, USDT_FANTOM, WBTC_FANTOM],
  [ChainId.BSC]: [...WETH_ONLY[ChainId.BSC], DAI_BSC, USDC_BSC, USDT_BSC],
  [ChainId.XDAI]: [...WETH_ONLY[ChainId.XDAI], WETH_XDAI, USDC_XDAI, USDT_XDAI, WBTC_XDAI],
  [ChainId.HARMONY]: [...WETH_ONLY[ChainId.HARMONY], DAI_HARMONY, WETH_HARMONY, USDC_HARMONY, USDT_HARMONY],
  [ChainId.TELOS]: [...WETH_ONLY[ChainId.TELOS], USDC_TELOS, USDT_TELOS],
  [ChainId.FUSE]: [...WETH_ONLY[ChainId.FUSE], DAI_FUSE, WETH_FUSE, USDC_FUSE, USDT_FUSE, WBTC_FUSE],
  [ChainId.OKEX]: [...WETH_ONLY[ChainId.OKEX], DAI_OKEX, USDC_OKEX, USDT_OKEX, WBTC_OKEX]
}

export const ADDITIONAL_BASES: { [chainId in ChainId]?: { [tokenAddress: string]: Token[] } } = {
  [ChainId.MAINNET]: {
    '0xA948E86885e12Fb09AfEF8C52142EBDbDf73cD18': [new Token(ChainId.MAINNET, UNI_ADDRESS, 18, 'UNI', 'Uniswap')],
    '0x561a4717537ff4AF5c687328c0f7E90a319705C0': [new Token(ChainId.MAINNET, UNI_ADDRESS, 18, 'UNI', 'Uniswap')],
    [FEI.address]: [TRIBE],
    [TRIBE.address]: [FEI],
    [FRAX.address]: [FXS],
    [FXS.address]: [FRAX],
    [WBTC.address]: [renBTC],
    [renBTC.address]: [WBTC]
  }
}

/**
 * Some tokens can only be swapped via certain pairs, so we override the list of bases that are considered for these
 * tokens.
 */
export const CUSTOM_BASES: { [chainId in ChainId]?: { [tokenAddress: string]: Token[] } } = {
  [ChainId.MAINNET]: {
    [AMPL.address]: [DAI, WETH[ChainId.MAINNET]]
  }
}

// used for display in the default list when adding liquidity
export const SUGGESTED_BASES: ChainTokenList = {
  ...WETH_ONLY,
  [ChainId.MAINNET]: [...WETH_ONLY[ChainId.MAINNET], DAI, USDC, USDT, WBTC],
  [ChainId.MATIC]: [...WETH_ONLY[ChainId.MATIC], DAI_POLYGON, WETH_POLYGON, USDC_POLYGON, USDT_POLYGON, WBTC_POLYGON],
  [ChainId.ARBITRUM]: [
    ...WETH_ONLY[ChainId.ARBITRUM],
    DAI_ARBITRUM,
    WETH_ARBITRUM,
    USDC_ARBITRUM,
    USDT_ARBITRUM,
    WBTC_ARBITRUM
  ],
  [ChainId.AVALANCHE]: [
    ...WETH_ONLY[ChainId.AVALANCHE],
    DAI_AVALANCHE,
    WETH_AVALANCHE,
    USDC_AVALANCHE,
    USDT_AVALANCHE,
    WBTC_AVALANCHE
  ],
  [ChainId.MOONRIVER]: [
    ...WETH_ONLY[ChainId.MOONRIVER],
    DAI_MOONRIVER,
    WETH_MOONRIVER,
    USDC_MOONRIVER,
    USDT_MOONRIVER,
    WBTC_MOONRIVER
  ],
  [ChainId.FANTOM]: [...WETH_ONLY[ChainId.FANTOM], DAI_FANTOM, WETH_FANTOM, USDC_FANTOM, USDT_FANTOM, WBTC_FANTOM],
  [ChainId.BSC]: [...WETH_ONLY[ChainId.BSC], DAI_BSC, USDC_BSC, USDT_BSC],
  [ChainId.XDAI]: [...WETH_ONLY[ChainId.XDAI], WETH_XDAI, USDC_XDAI, USDT_XDAI, WBTC_XDAI],
  [ChainId.HARMONY]: [...WETH_ONLY[ChainId.HARMONY], DAI_HARMONY, WETH_HARMONY, USDC_HARMONY, USDT_HARMONY],
  [ChainId.TELOS]: [...WETH_ONLY[ChainId.TELOS], USDC_TELOS, USDT_TELOS],
  [ChainId.FUSE]: [...WETH_ONLY[ChainId.FUSE], DAI_FUSE, WETH_FUSE, USDC_FUSE, USDT_FUSE, WBTC_FUSE],
  [ChainId.OKEX]: [...WETH_ONLY[ChainId.OKEX], DAI_OKEX, USDC_OKEX, USDT_OKEX, WBTC_OKEX]
}

// used to construct the list of all pairs we consider by default in the frontend
export const BASES_TO_TRACK_LIQUIDITY_FOR: ChainTokenList = {
  ...WETH_ONLY,
  [ChainId.MAINNET]: [...WETH_ONLY[ChainId.MAINNET], DAI, USDC, USDT, WBTC],
  [ChainId.MATIC]: [...WETH_ONLY[ChainId.MATIC], DAI_POLYGON, WETH_POLYGON, USDC_POLYGON, USDT_POLYGON, WBTC_POLYGON],
  [ChainId.ARBITRUM]: [
    ...WETH_ONLY[ChainId.ARBITRUM],
    DAI_ARBITRUM,
    WETH_ARBITRUM,
    USDC_ARBITRUM,
    USDT_ARBITRUM,
    WBTC_ARBITRUM
  ],
  [ChainId.AVALANCHE]: [
    ...WETH_ONLY[ChainId.AVALANCHE],
    DAI_AVALANCHE,
    WETH_AVALANCHE,
    USDC_AVALANCHE,
    USDT_AVALANCHE,
    WBTC_AVALANCHE
  ],
  [ChainId.MOONRIVER]: [
    ...WETH_ONLY[ChainId.MOONRIVER],
    DAI_MOONRIVER,
    WETH_MOONRIVER,
    USDC_MOONRIVER,
    USDT_MOONRIVER,
    WBTC_MOONRIVER
  ],
  [ChainId.FANTOM]: [...WETH_ONLY[ChainId.FANTOM], DAI_FANTOM, WETH_FANTOM, USDC_FANTOM, USDT_FANTOM, WBTC_FANTOM],
  [ChainId.BSC]: [...WETH_ONLY[ChainId.BSC], DAI_BSC, USDC_BSC, USDT_BSC],
  [ChainId.XDAI]: [...WETH_ONLY[ChainId.XDAI], WETH_XDAI, USDC_XDAI, USDT_XDAI, WBTC_XDAI],
  [ChainId.HARMONY]: [...WETH_ONLY[ChainId.HARMONY], DAI_HARMONY, WETH_HARMONY, USDC_HARMONY, USDT_HARMONY],
  [ChainId.TELOS]: [...WETH_ONLY[ChainId.TELOS], USDC_TELOS, USDT_TELOS],
  [ChainId.FUSE]: [...WETH_ONLY[ChainId.FUSE], DAI_FUSE, WETH_FUSE, USDC_FUSE, USDT_FUSE, WBTC_FUSE],
  [ChainId.OKEX]: [...WETH_ONLY[ChainId.OKEX], DAI_OKEX, USDC_OKEX, USDT_OKEX, WBTC_OKEX]
}

export const PINNED_PAIRS: { readonly [chainId in ChainId]?: [Token, Token][] } = {
  [ChainId.MAINNET]: [
    [
      new Token(ChainId.MAINNET, '0x5d3a536E4D6DbD6114cc1Ead35777bAB948E3643', 8, 'cDAI', 'Compound Dai'),
      new Token(ChainId.MAINNET, '0x39AA39c021dfbaE8faC545936693aC917d5E7563', 8, 'cUSDC', 'Compound USD Coin')
    ],
    [USDC, USDT],
    [DAI, USDT]
  ],
  [ChainId.MATIC]: [],
  [ChainId.MOONRIVER]: []
}

export interface WalletInfo {
  connector?: AbstractConnector
  name: string
  iconName: string
  description: string
  href: string | null
  color: string
  primary?: true
  mobile?: true
  mobileOnly?: true
}

export const SUPPORTED_WALLETS: { [key: string]: WalletInfo } = {
  INJECTED: {
    connector: injected,
    name: 'Injected',
    iconName: 'arrow-right.svg',
    description: 'Injected web3 provider.',
    href: null,
    color: '#010101',
    primary: true
  },
  METAMASK: {
    connector: injected,
    name: 'MetaMask',
    iconName: 'metamask.png',
    description: 'Easy-to-use browser extension.',
    href: null,
    color: '#E8831D'
  },
  WALLET_CONNECT: {
    connector: walletconnect,
    name: 'WalletConnect',
    iconName: 'walletConnectIcon.svg',
    description: 'Connect to Trust Wallet, Rainbow Wallet and more...',
    href: null,
    color: '#4196FC',
    mobile: true
  },
  WALLET_LINK: {
    connector: walletlink,
    name: 'Coinbase Wallet',
    iconName: 'coinbaseWalletIcon.svg',
    description: 'Use Coinbase Wallet app on mobile device',
    href: null,
    color: '#315CF5'
  },
  COINBASE_LINK: {
    name: 'Open in Coinbase Wallet',
    iconName: 'coinbaseWalletIcon.svg',
    description: 'Open in Coinbase Wallet app.',
    href: 'https://go.cb-w.com/mtUDhEZPy1',
    color: '#315CF5',
    mobile: true,
    mobileOnly: true
  },
  FORTMATIC: {
    connector: fortmatic,
    name: 'Fortmatic',
    iconName: 'fortmaticIcon.png',
    description: 'Login using Fortmatic hosted wallet',
    href: null,
    color: '#6748FF',
    mobile: true
  },
  Portis: {
    connector: portis,
    name: 'Portis',
    iconName: 'portisIcon.png',
    description: 'Login using Portis hosted wallet',
    href: null,
    color: '#4A6C9B',
    mobile: true
  }
}

export const NetworkContextName = 'NETWORK'

// default allowed slippage, in bips
export const INITIAL_ALLOWED_SLIPPAGE = 50
// 20 minutes, denominated in seconds
export const DEFAULT_DEADLINE_FROM_NOW = 60 * 20

// used for rewards deadlines
export const BIG_INT_SECONDS_IN_WEEK = JSBI.BigInt(60 * 60 * 24 * 7)

export const BIG_INT_ZERO = JSBI.BigInt(0)

// one basis point
export const ONE_BIPS = new Percent(JSBI.BigInt(1), JSBI.BigInt(10000))
export const BIPS_BASE = JSBI.BigInt(10000)
// used for warning states
export const ALLOWED_PRICE_IMPACT_LOW: Percent = new Percent(JSBI.BigInt(100), BIPS_BASE) // 1%
export const ALLOWED_PRICE_IMPACT_MEDIUM: Percent = new Percent(JSBI.BigInt(300), BIPS_BASE) // 3%
export const ALLOWED_PRICE_IMPACT_HIGH: Percent = new Percent(JSBI.BigInt(500), BIPS_BASE) // 5%
// if the price slippage exceeds this number, force the user to type 'confirm' to execute
export const PRICE_IMPACT_WITHOUT_FEE_CONFIRM_MIN: Percent = new Percent(JSBI.BigInt(1000), BIPS_BASE) // 10%
// for non expert mode disable swaps above this
export const BLOCKED_PRICE_IMPACT_NON_EXPERT: Percent = new Percent(JSBI.BigInt(1500), BIPS_BASE) // 15%

// used to ensure the user doesn't send so much ETH so they end up with <.01
export const MIN_ETH: JSBI = JSBI.exponentiate(JSBI.BigInt(10), JSBI.BigInt(16)) // .01 ETH
export const BETTER_TRADE_LESS_HOPS_THRESHOLD = new Percent(JSBI.BigInt(50), JSBI.BigInt(10000))

export const ZERO_PERCENT = new Percent('0')
export const ONE_HUNDRED_PERCENT = new Percent('1')

// SDN OFAC addresses
export const BLOCKED_ADDRESSES: string[] = [
  '0x7F367cC41522cE07553e823bf3be79A889DEbe1B',
  '0xd882cFc20F52f2599D84b8e8D58C7FB62cfE344b',
  '0x901bb9583b24D97e995513C6778dc6888AB6870e',
  '0xA7e5d5A720f06526557c513402f2e6B5fA20b008',
  '0x8576aCC5C05D6Ce88f4e49bf65BdF0C62F91353C'
]

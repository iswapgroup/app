// used to mark unsupported tokens, these are hosted lists of unsupported tokens

const UNSUPPORTED_LIST_URLS: string[] = []
//const DEFAULT_LIST_OF_LISTS: string[] = []
//const DEFAULT_ACTIVE_LIST_URLS: string[] = []

const COMPOUND_LIST = 'https://raw.githubusercontent.com/compound-finance/token-list/master/compound.tokenlist.json'
const UMA_LIST = 'https://umaproject.org/uma.tokenlist.json'
const AAVE_LIST = 'tokenlist.aave.eth'
const SYNTHETIX_LIST = 'synths.snx.eth'
const WRAPPED_LIST = 'wrapped.tokensoft.eth'
const SET_LIST = 'https://raw.githubusercontent.com/SetProtocol/uniswap-tokenlist/main/set.tokenlist.json'
const OPYN_LIST = 'https://raw.githubusercontent.com/opynfinance/opyn-tokenlist/master/opyn-v1.tokenlist.json'
const ROLL_LIST = 'https://app.tryroll.com/tokens.json'
const COINGECKO_LIST = 'https://tokens.coingecko.com/uniswap/all.json'
const CMC_ALL_LIST = 'defi.cmc.eth'
const CMC_STABLECOIN = 'stablecoin.cmc.eth'
const KLEROS_LIST = 't2crtokens.eth'
const GEMINI_LIST = 'https://www.gemini.com/uniswap/manifest.json'
//const BA_LIST = 'https://raw.githubusercontent.com/The-Blockchain-Association/sec-notice-list/master/ba-sec-list.json'

//UNSUPPORTED_LIST_URLS_ETH = [BA_LIST]

// lower index == higher priority for token import
const DEFAULT_LIST_OF_LISTS_ETH = [
  COMPOUND_LIST,
  AAVE_LIST,
  SYNTHETIX_LIST,
  UMA_LIST,
  WRAPPED_LIST,
  SET_LIST,
  OPYN_LIST,
  ROLL_LIST,
  COINGECKO_LIST,
  CMC_ALL_LIST,
  CMC_STABLECOIN,
  KLEROS_LIST,
  GEMINI_LIST
  // ...UNSUPPORTED_LIST_URLS_ETH // need to load unsupported tokens as well
]
// default lists to be 'active' aka searched across
const DEFAULT_ACTIVE_LIST_URLS = [GEMINI_LIST]

//const PANCAKE_TOP100 = 'https://tokens.pancakeswap.finance/pancakeswap-top-100.json'
const PANCAKE_EXTENDED = 'https://tokens.pancakeswap.finance/pancakeswap-extended.json'
const DEFAULT_LIST_OF_LISTS_BSC = [PANCAKE_EXTENDED]

/*const MATIC_LIST = 'https://unpkg.com/quickswap-default-token-list@1.2.20/build/quickswap-default.tokenlist.json'
const DEFAULT_LIST_OF_LISTS_MATIC = [MATIC_LIST]

const ARBITRUM_LIST = 'https://bridge.arbitrum.io/token-list-42161.json'
const DEFAULT_LIST_OF_LISTS_ARBITRUM = [ARBITRUM_LIST]

const AVALANCHE_LIST = 'https://raw.githubusercontent.com/pangolindex/tokenlists/main/ab.tokenlist.json'
const DEFAULT_LIST_OF_LISTS_AVALANCHE = [AVALANCHE_LIST];*/

//const MOONRIVER_COMMUNITY_LIST =
//'https://raw.githubusercontent.com/solarbeamio/solarbeam-tokenlist/main/community.tokenlist.json'

//const DEFAULT_LIST_OF_LISTS_MOONRIVER = [MOONRIVER_COMMUNITY_LIST]

export const DEFAULT_LISTS = {
  1: DEFAULT_LIST_OF_LISTS_ETH,
  56: DEFAULT_LIST_OF_LISTS_BSC
  //1285: DEFAULT_LIST_OF_LISTS_MOONRIVER
}

const DEFAULT_LIST_OF_LISTS = [...DEFAULT_LIST_OF_LISTS_ETH]
//DEFAULT_LIST_OF_LISTS.push(PANCAKE_TOP100)
DEFAULT_LIST_OF_LISTS.push(PANCAKE_EXTENDED)
//DEFAULT_LIST_OF_LISTS.push(MOONRIVER_COMMUNITY_LIST)
/*DEFAULT_LIST_OF_LISTS.push(MATIC_LIST)
DEFAULT_LIST_OF_LISTS.push(ARBITRUM_LIST)
DEFAULT_LIST_OF_LISTS.push(AVALANCHE_LIST)*/

export { UNSUPPORTED_LIST_URLS, DEFAULT_LIST_OF_LISTS, DEFAULT_ACTIVE_LIST_URLS }

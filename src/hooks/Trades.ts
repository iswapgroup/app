import { isTradeBetter } from 'utils/trades'
//import { Currency, CurrencyAmount, Pair, Token, Trade } from '@uniswap/sdk'
import { Currency, CurrencyAmount, Pair, Token, Trade } from '../lib/'
import { Pair as PairUniswap, Trade as TradeUniswap } from '../libUniswap/'
import flatMap from 'lodash.flatmap'
import { useMemo } from 'react'

import {
  BASES_TO_CHECK_TRADES_AGAINST,
  CUSTOM_BASES,
  BETTER_TRADE_LESS_HOPS_THRESHOLD,
  ADDITIONAL_BASES
} from '../constants'
import { PairState, usePairs, usePairsUniswap } from '../data/Reserves'
import { wrappedCurrency } from '../utils/wrappedCurrency'

import { useActiveWeb3React } from './index'
import { useUnsupportedTokens } from './Tokens'
import { useUserSingleHopOnly } from 'state/user/hooks'

function useAllCommonPairs(currencyA?: Currency, currencyB?: Currency): Pair[] {
  const { chainId } = useActiveWeb3React()

  const [tokenA, tokenB] = chainId
    ? [wrappedCurrency(currencyA, chainId), wrappedCurrency(currencyB, chainId)]
    : [undefined, undefined]

  const bases: Token[] = useMemo(() => {
    if (!chainId) return []

    const common = BASES_TO_CHECK_TRADES_AGAINST[chainId] ?? []
    const additionalA = tokenA ? ADDITIONAL_BASES[chainId]?.[tokenA.address] ?? [] : []
    const additionalB = tokenB ? ADDITIONAL_BASES[chainId]?.[tokenB.address] ?? [] : []

    return [...common, ...additionalA, ...additionalB]
  }, [chainId, tokenA, tokenB])

  const basePairs: [Token, Token][] = useMemo(
    () => flatMap(bases, (base): [Token, Token][] => bases.map(otherBase => [base, otherBase])),
    [bases]
  )

  const allPairCombinations: [Token, Token][] = useMemo(
    () =>
      tokenA && tokenB
        ? [
            // the direct pair
            [tokenA, tokenB],
            // token A against all bases
            ...bases.map((base): [Token, Token] => [tokenA, base]),
            // token B against all bases
            ...bases.map((base): [Token, Token] => [tokenB, base]),
            // each base against all bases
            ...basePairs
          ]
            .filter((tokens): tokens is [Token, Token] => Boolean(tokens[0] && tokens[1]))
            .filter(([t0, t1]) => t0.address !== t1.address)
            .filter(([tokenA, tokenB]) => {
              if (!chainId) return true
              const customBases = CUSTOM_BASES[chainId]

              const customBasesA: Token[] | undefined = customBases?.[tokenA.address]
              const customBasesB: Token[] | undefined = customBases?.[tokenB.address]

              if (!customBasesA && !customBasesB) return true

              if (customBasesA && !customBasesA.find(base => tokenB.equals(base))) return false
              if (customBasesB && !customBasesB.find(base => tokenA.equals(base))) return false

              return true
            })
        : [],
    [tokenA, tokenB, bases, basePairs, chainId]
  )

  const allPairs = usePairs(allPairCombinations)

  // only pass along valid pairs, non-duplicated pairs
  return useMemo(
    () =>
      Object.values(
        allPairs
          // filter out invalid pairs
          .filter((result): result is [PairState.EXISTS, Pair] => Boolean(result[0] === PairState.EXISTS && result[1]))
          // filter out duplicated pairs
          .reduce<{ [pairAddress: string]: Pair }>((memo, [, curr]) => {
            memo[curr.liquidityToken.address] = memo[curr.liquidityToken.address] ?? curr
            return memo
          }, {})
      ),
    [allPairs]
  )
}
function useAllCommonPairsUniswap(currencyA?: Currency, currencyB?: Currency): Pair[] {
  const { chainId } = useActiveWeb3React()

  const [tokenA, tokenB] = chainId
    ? [wrappedCurrency(currencyA, chainId), wrappedCurrency(currencyB, chainId)]
    : [undefined, undefined]

  const bases: Token[] = useMemo(() => {
    if (!chainId) return []

    const common = BASES_TO_CHECK_TRADES_AGAINST[chainId] ?? []
    const additionalA = tokenA ? ADDITIONAL_BASES[chainId]?.[tokenA.address] ?? [] : []
    const additionalB = tokenB ? ADDITIONAL_BASES[chainId]?.[tokenB.address] ?? [] : []

    return [...common, ...additionalA, ...additionalB]
  }, [chainId, tokenA, tokenB])

  const basePairs: [Token, Token][] = useMemo(
    () => flatMap(bases, (base): [Token, Token][] => bases.map(otherBase => [base, otherBase])),
    [bases]
  )

  const allPairCombinations: [Token, Token][] = useMemo(
    () =>
      tokenA && tokenB
        ? [
            // the direct pair
            [tokenA, tokenB],
            // token A against all bases
            ...bases.map((base): [Token, Token] => [tokenA, base]),
            // token B against all bases
            ...bases.map((base): [Token, Token] => [tokenB, base]),
            // each base against all bases
            ...basePairs
          ]
            .filter((tokens): tokens is [Token, Token] => Boolean(tokens[0] && tokens[1]))
            .filter(([t0, t1]) => t0.address !== t1.address)
            .filter(([tokenA, tokenB]) => {
              if (!chainId) return true
              const customBases = CUSTOM_BASES[chainId]

              const customBasesA: Token[] | undefined = customBases?.[tokenA.address]
              const customBasesB: Token[] | undefined = customBases?.[tokenB.address]

              if (!customBasesA && !customBasesB) return true

              if (customBasesA && !customBasesA.find(base => tokenB.equals(base))) return false
              if (customBasesB && !customBasesB.find(base => tokenA.equals(base))) return false

              return true
            })
        : [],
    [tokenA, tokenB, bases, basePairs, chainId]
  )

  //console.log('useAllCommonPairsUniswap: allPairCombinations', allPairCombinations)
  const allPairs = usePairsUniswap(allPairCombinations)
  //console.log('useAllCommonPairsUniswap: allPairs', allPairs)

  // only pass along valid pairs, non-duplicated pairs
  return useMemo(
    () =>
      Object.values(
        allPairs
          // filter out invalid pairs
          .filter((result): result is [PairState.EXISTS, Pair] => Boolean(result[0] === PairState.EXISTS && result[1]))
          // filter out duplicated pairs
          .reduce<{ [pairAddress: string]: Pair }>((memo, [, curr]) => {
            memo[curr.liquidityToken.address] = memo[curr.liquidityToken.address] ?? curr
            return memo
          }, {})
      ),
    [allPairs]
  )
}

const MAX_HOPS = 3

/**
 * Returns the best trade for the exact amount of tokens in to the given token out
 **/
export function useTradeExactIn(
  currencyAmountIn?: CurrencyAmount,
  currencyOut?: Currency
): Trade | TradeUniswap | null | undefined {
  const allowedPairs = useAllCommonPairs(currencyAmountIn?.currency, currencyOut)
  const allowedPairsUniswap = useAllCommonPairsUniswap(currencyAmountIn?.currency, currencyOut)
  //console.log('allowedPairs', allowedPairs)
  //console.log('allowedPairsUniswap', allowedPairsUniswap)

  const [singleHopOnly] = useUserSingleHopOnly()
  return useMemo(() => {
    let tradeIswapError = null
    let tradeUniswapError = null
    let returnData = null

    if (currencyAmountIn && currencyOut) {
      //console.log('currencyAmountIn && currencyOut')

      let bestTradeSoFar: Trade | null = null

      if (allowedPairs.length > 0) {
        //console.log('allowedPairs.length')

        try {
          if (singleHopOnly) {
            bestTradeSoFar =
              Trade.bestTradeExactIn(allowedPairs, currencyAmountIn, currencyOut, {
                maxHops: 1,
                maxNumResults: 1
              })[0] ?? null
          }
          for (let i = 1; i <= MAX_HOPS; i++) {
            const currentTrade: Trade | null =
              Trade.bestTradeExactIn(allowedPairs, currencyAmountIn, currencyOut, {
                maxHops: i,
                maxNumResults: 1
              })[0] ?? null
            // if current trade is best yet, save it
            if (isTradeBetter(bestTradeSoFar, currentTrade, BETTER_TRADE_LESS_HOPS_THRESHOLD)) {
              bestTradeSoFar = currentTrade
            }
          }
        } catch (e) {
          tradeIswapError = e
        }

        /*console.log(
          'bestTradeSoFar iswap',
          bestTradeSoFar,
          bestTradeSoFar?.outputAmount.toSignificant(10),
          bestTradeSoFar?.priceImpact.toSignificant(10)
        )*/
      }

      //uniswap
      let bestTradeSoFarUniswap: TradeUniswap | null = null

      if (allowedPairsUniswap.length > 0) {
        try {
          if (singleHopOnly) {
            bestTradeSoFarUniswap =
              TradeUniswap.bestTradeExactIn(
                (allowedPairsUniswap as unknown) as PairUniswap[],
                currencyAmountIn,
                currencyOut,
                {
                  maxHops: 1,
                  maxNumResults: 1
                }
              )[0] ?? null
          } else {
            // search through trades with varying hops, find best trade out of them

            for (let i = 1; i <= MAX_HOPS; i++) {
              // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
              // @ts-ignore
              const currentTrade: TradeUniswap | null =
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                TradeUniswap.bestTradeExactIn(
                  (allowedPairsUniswap as unknown) as PairUniswap[],
                  currencyAmountIn,
                  currencyOut,
                  {
                    maxHops: i,
                    maxNumResults: 1
                  }
                )[0] ?? null
              // if current trade is best yet, save it
              if (isTradeBetter(bestTradeSoFarUniswap, currentTrade, BETTER_TRADE_LESS_HOPS_THRESHOLD)) {
                bestTradeSoFarUniswap = currentTrade
              }
            }
          }
        } catch (e) {
          tradeUniswapError = e
        }

        /*console.log(
          'bestTradeSoFar Uniswap',
          bestTradeSoFarUniswap,
          bestTradeSoFarUniswap?.outputAmount.toSignificant(10),
          bestTradeSoFarUniswap?.priceImpact.toSignificant(10)
        )*/
      }

      let bestTrade = null

      if (bestTradeSoFar && bestTradeSoFarUniswap) {
        const iswapImpact = parseFloat(bestTradeSoFar.priceImpact.toSignificant(10))
        const iswapOutputAmount = parseFloat(bestTradeSoFar.outputAmount.toSignificant(10))
        const uniswapImpact = parseFloat(bestTradeSoFarUniswap.priceImpact.toSignificant(10))
        const uniswapOutputAmount = parseFloat(bestTradeSoFarUniswap.outputAmount.toSignificant(10))

        if (iswapImpact <= 15) {
          returnData = bestTradeSoFar
          bestTrade = bestTradeSoFar
        } else if (uniswapImpact > iswapImpact && uniswapOutputAmount > iswapOutputAmount) {
          returnData = bestTradeSoFarUniswap
          bestTrade = bestTradeSoFarUniswap
        } else if (iswapImpact <= uniswapImpact) {
          returnData = bestTradeSoFar
          bestTrade = bestTradeSoFar
        } else if (iswapImpact > uniswapImpact) {
          returnData = bestTradeSoFarUniswap
          bestTrade = bestTradeSoFarUniswap
        }
      }

      if (!bestTrade) {
        if (!bestTradeSoFar && bestTradeSoFarUniswap) {
          returnData = bestTradeSoFarUniswap
        } else if (bestTradeSoFar && !bestTradeSoFarUniswap) {
          returnData = bestTradeSoFar
        } else if (tradeIswapError && !tradeUniswapError) {
          returnData = bestTradeSoFarUniswap
        } else if (!tradeIswapError && !tradeUniswapError) {
          returnData = undefined
        }
      }
    }

    return returnData ?? undefined
  }, [allowedPairs, allowedPairsUniswap, currencyAmountIn, currencyOut, singleHopOnly])
}

/**
 * Returns the best trade for the token in to the exact amount of token out
 */
export function useTradeExactOut(
  currencyIn?: Currency,
  currencyAmountOut?: CurrencyAmount
): Trade | TradeUniswap | null | undefined {
  const allowedPairs = useAllCommonPairs(currencyIn, currencyAmountOut?.currency)
  const allowedPairsUniswap = useAllCommonPairsUniswap(currencyIn, currencyAmountOut?.currency)
  //console.log('useTradeExactOut: allowedPairs', allowedPairs)
  //console.log('useTradeExactOut: allowedPairsUniswap', allowedPairsUniswap)

  const [singleHopOnly] = useUserSingleHopOnly()

  return useMemo(() => {
    let returnData = null

    if (currencyIn && currencyAmountOut) {
      let bestTradeSoFar: Trade | null = null

      if (allowedPairs.length > 0) {
        if (singleHopOnly) {
          bestTradeSoFar =
            Trade.bestTradeExactOut(allowedPairs, currencyIn, currencyAmountOut, {
              maxHops: 1,
              maxNumResults: 1
            })[0] ?? null
        }
        // search through trades with varying hops, find best trade out of them

        for (let i = 1; i <= MAX_HOPS; i++) {
          const currentTrade =
            Trade.bestTradeExactOut(allowedPairs, currencyIn, currencyAmountOut, {
              maxHops: i,
              maxNumResults: 1
            })[0] ?? null
          if (isTradeBetter(bestTradeSoFar, currentTrade, BETTER_TRADE_LESS_HOPS_THRESHOLD)) {
            bestTradeSoFar = currentTrade
          }
        }
      }

      let bestTradeSoFarUniswap: TradeUniswap | null = null

      if (allowedPairsUniswap.length > 0) {
        if (singleHopOnly) {
          bestTradeSoFarUniswap =
            TradeUniswap.bestTradeExactOut(
              (allowedPairsUniswap as unknown) as PairUniswap[],
              currencyIn,
              currencyAmountOut,
              {
                maxHops: 1,
                maxNumResults: 1
              }
            )[0] ?? null
        }
        // search through trades with varying hops, find best trade out of them

        for (let i = 1; i <= MAX_HOPS; i++) {
          const currentTrade =
            TradeUniswap.bestTradeExactOut(
              (allowedPairsUniswap as unknown) as PairUniswap[],
              currencyIn,
              currencyAmountOut,
              {
                maxHops: i,
                maxNumResults: 1
              }
            )[0] ?? null
          if (isTradeBetter(bestTradeSoFarUniswap, currentTrade, BETTER_TRADE_LESS_HOPS_THRESHOLD)) {
            bestTradeSoFarUniswap = currentTrade
          }
        }

        /*console.log(
          'bestTradeSoFar Uniswap',
          bestTradeSoFarUniswap,
          bestTradeSoFarUniswap?.inputAmount.toSignificant(10),
          bestTradeSoFarUniswap?.priceImpact.toSignificant(10)
        )*/
      }

      let bestTrade = null

      if (bestTradeSoFar && bestTradeSoFarUniswap) {
        const iswapImpact = parseFloat(bestTradeSoFar.priceImpact.toSignificant(10))
        const iswapInputAmount = parseFloat(bestTradeSoFar.inputAmount.toSignificant(10))
        const uniswapImpact = parseFloat(bestTradeSoFarUniswap.priceImpact.toSignificant(10))
        const uniswapInputAmount = parseFloat(bestTradeSoFarUniswap.inputAmount.toSignificant(10))

        if (iswapImpact <= 15) {
          returnData = bestTradeSoFar
          bestTrade = bestTradeSoFar
        } else if (uniswapImpact > iswapImpact && uniswapInputAmount < iswapInputAmount) {
          returnData = bestTradeSoFarUniswap
          bestTrade = bestTradeSoFarUniswap
        } else if (iswapImpact <= uniswapImpact) {
          returnData = bestTradeSoFar
          bestTrade = bestTradeSoFar
        } else if (iswapImpact > uniswapImpact) {
          returnData = bestTradeSoFarUniswap
          bestTrade = bestTradeSoFarUniswap
        }
      }

      if (!bestTrade) {
        if (!bestTradeSoFar && bestTradeSoFarUniswap) {
          returnData = bestTradeSoFarUniswap
        } else if (bestTradeSoFar && !bestTradeSoFarUniswap) {
          returnData = bestTradeSoFar
        }
      }
    }

    return returnData ?? undefined
  }, [currencyIn, currencyAmountOut, allowedPairs, allowedPairsUniswap, singleHopOnly])
}

export function useIsTransactionUnsupported(currencyIn?: Currency, currencyOut?: Currency): boolean {
  const unsupportedTokens: { [address: string]: Token } = useUnsupportedTokens()
  const { chainId } = useActiveWeb3React()

  const tokenIn = wrappedCurrency(currencyIn, chainId)
  const tokenOut = wrappedCurrency(currencyOut, chainId)

  // if unsupported list loaded & either token on list, mark as unsupported
  if (unsupportedTokens) {
    if (tokenIn && Object.keys(unsupportedTokens).includes(tokenIn.address)) {
      return true
    }
    if (tokenOut && Object.keys(unsupportedTokens).includes(tokenOut.address)) {
      return true
    }
  }

  return false
}

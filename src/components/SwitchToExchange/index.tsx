import { darken } from 'polished'
import React from 'react'
import styled, { css } from 'styled-components'
import { ButtonSecondary } from '../Button'

const Generic = styled(ButtonSecondary)`
  ${({ theme }) => theme.flexRowNoWrap}
  width: 100%;
  align-items: center;
  padding: 0.5rem;
  border-radius: 8px;
  cursor: pointer;
  user-select: none;
  :focus {
    outline: none;
  }
`

const SwitchToExchangeEl = styled(Generic)<{ faded?: boolean }>`
  background-color: transparent;
  border: none;
  color: #6a7981;
  font-weight: 500;

  :hover,
  :focus {
    border: 1px solid ${({ theme }) => darken(0.05, theme.primary4)};
    color: #6a7981;
  }

  ${({ faded }) =>
    faded &&
    css`
      background-color: transparent;
      border: 1px solid ${({ theme }) => theme.primary5};
      color: #6a7981;

      :hover,
      :focus {
        border: 1px solid ${({ theme }) => darken(0.05, theme.primary4)};
        color: #6a7981;
      }
    `}
`

const Text = styled.p`
  flex: 1 1 auto;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin: 0 0.5rem 0 0.25rem;
  font-size: 1rem;
  width: fit-content;
  font-weight: 500;

  ${({ theme }) => theme.mediaWidth.upToSmall`
    display:none
  `};
`

export default function SwitchToExchange() {
  return (
    <>
      <SwitchToExchangeEl faded={true}>
        <Text>Switch to exchange</Text>
      </SwitchToExchangeEl>
    </>
  )
}

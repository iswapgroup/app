import { darken } from 'polished'
import React from 'react'
import { isMobile } from 'react-device-detect'
import styled, { css } from 'styled-components'
import { ButtonSecondary } from '../Button'
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import Cookies from 'js-cookie'
//import { useLocation } from 'react-router'

const Generic = styled(ButtonSecondary)`
  ${({ theme }) => theme.flexRowNoWrap}
  width: 100%;
  align-items: center;
  padding: 0.5rem;
  border-radius: 8px;
  cursor: pointer;
  user-select: none;
  :focus {
    outline: none;
  }
`

const AnimationEl = styled(Generic)<{ faded?: boolean }>`
  background-color: ${({ theme }) => theme.primary4};
  border: none;
  color: ${({ theme }) => theme.primaryText1};
  font-weight: 500;

  :hover,
  :focus {
    border: 1px solid ${({ theme }) => darken(0.05, theme.primary4)};
    color: ${({ theme }) => theme.primaryText1};
  }

  ${({ faded }) =>
    faded &&
    css`
      background-color: ${({ theme }) => theme.primary5};
      border: 1px solid ${({ theme }) => theme.primary5};
      color: ${({ theme }) => theme.primaryText1};

      :hover,
      :focus {
        border: 1px solid ${({ theme }) => darken(0.05, theme.primary4)};
        color: ${({ theme }) => darken(0.05, theme.primaryText1)};
      }
    `}
`

const Text = styled.p`
  flex: 1 1 auto;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin: 0 0.5rem 0 0.25rem;
  font-size: 1rem;
  width: fit-content;
  font-weight: 500;
`

const TextAnimation = styled.p`
  flex: 1 1 auto;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin: 0 0.05rem 0 0.25rem;
  font-size: 1rem;
  width: fit-content;
  font-weight: 500;

  ${({ theme }) => theme.mediaWidth.upToSmall`
    display:none
  `};
`

export default function Animation() {
  function isAnimationActive() {
    return Cookies.get('animation_haze_disabled') === undefined
  }

  function detectWebGL() {
    // Check for the WebGL rendering context
    if (window.WebGLRenderingContext) {
      const canvas = document.createElement('canvas')
      const names = ['webgl', 'experimental-webgl', 'moz-webgl', 'webkit-3d']
      let context = false

      for (const i in names) {
        try {
          // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
          // @ts-ignore
          context = canvas.getContext(names[i])
          // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
          // @ts-ignore
          if (context && typeof context.getParameter === 'function') {
            // WebGL is enabled.
            return true
          }
          // eslint-disable-next-line no-empty
        } catch (e) {}
      }

      // WebGL is supported, but disabled.
      return false
    }

    // WebGL not supported.
    return false
  }

  function SetAnimationToggle() {
    if (isAnimationActive()) {
      Cookies.set('animation_haze_disabled', 1, { expires: 365 })
      window.location.reload()
    } else {
      Cookies.remove('animation_haze_disabled')
      window.location.reload()
    }
  }

  return (
    <>
      {detectWebGL() && !isMobile && (
        <AnimationEl faded={true} onClick={SetAnimationToggle}>
          <TextAnimation>Animation</TextAnimation>
          <Text>{isAnimationActive() ? 'off' : 'on'}</Text>
        </AnimationEl>
      )}
    </>
  )
}

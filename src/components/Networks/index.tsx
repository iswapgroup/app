import React, { useState } from 'react'
import styled, { css } from 'styled-components'
import { ButtonSecondary } from '../Button'
import EthIcon from '../../assets/images/networks/eth.webp'
import BscIcon from '../../assets/images/networks/bsc.webp'
import GnosysIcon from '../../assets/images/networks/gnosis.webp'
import PolygonIcon from '../../assets/images/networks/polygon.webp'
import FantomIcon from '../../assets/images/networks/fantom.jpg'
import MoonriverIcon from '../../assets/images/networks/moonriver.webp'
import ArbitrumIcon from '../../assets/images/networks/arbitrum.webp'
import AvalanceIcon from '../../assets/images/networks/avalanche.webp'
//import HarmonyIcon from '../../assets/images/networks/harmonyone.jpg'
import TelosIcon from '../../assets/images/networks/telos.webp'
import FuseIcon from '../../assets/images/networks/fuse.webp'
import HecoIcon from '../../assets/images/networks/heco.jpg'
import OkexIcon from '../../assets/images/networks/okex.webp'
import Modal from '../Modal'
import Column, { AutoColumn } from '../Column'
import { RowBetween } from '../Row'
import { Text } from 'rebass'
import { CloseIcon } from '../../theme'
import { ChainId } from '../../lib'
import { useActiveWeb3React } from '../../hooks'
import { useWalletModalToggle } from '../../state/application/hooks'
import { darken } from 'polished'

export const PaddedColumn = styled(AutoColumn)`
  padding: 20px;
`
const Generic = styled(ButtonSecondary)`
  ${({ theme }) => theme.flexRowNoWrap}
  width: 100%;
  align-items: center;
  padding: 0.3rem;
  border-radius: 8px;
  cursor: pointer;
  user-select: none;
  :focus {
    outline: none;
  }
`

const NetworksEl = styled(Generic)<{ faded?: boolean }>`
  background-color: transparent;
  border: none;
  color: #6a7981;
  font-weight: 500;

  :hover,
  :focus {
    border: 1px solid ${({ theme }) => darken(0.05, theme.primary4)};
    color: #6a7981;
  }

  ${({ faded }) =>
    faded &&
    css`
      background-color: transparent;
      border: 1px solid ${({ theme }) => theme.primary5};
      color: #6a7981;

      :hover,
      :focus {
        border: 1px solid ${({ theme }) => darken(0.05, theme.primary4)};
        color: #6a7981;
      }
    `}
`

const NetworkImage = styled.img`
  height: 1.588rem;
  border-radius: 5px;
`

const Image = styled(NetworkImage)`
  position: absolute;
  right: 20px;
`

const NetworkSelectionImage = styled.img`
  width: 32px;
  height: 32px;
  border-radius: 5px;
`

const ContentWrapper = styled(Column)`
  width: 100%;
  flex: 1 1;
  position: relative;
`

const NetworksWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`

const NetworkSelectionImageWrapper = styled.div`
  margin-right: 20px;
  display: flex;
  align-items: center;

  ${({ theme }) => theme.mediaWidth.upToSmall`
     margin-right: initial !important;
  `};
`

const NetworkName = styled.div`
  ${({ theme }) => theme.mediaWidth.upToSmall`
    display:none
  `};
`

const NetworkRow = styled.div`
  display: flex;
  margin-right: 10px;
  min-width: calc(50% - 10px);
  max-width: 230px;
  margin-bottom: 10px;
  border: 1px solid #153d6f70;
  border-radius: 0.625rem;
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  padding-left: 1rem;
  padding-right: 1rem;
  align-items: center;
  cursor: pointer;
  background-color: rgba(0, 0, 0, 0.2);
  height: 58px;

  :hover {
    border-color: rgb(9 147 236);
  }

  &.active {
    border-color: rgb(167 85 221);
  }

  ${({ theme }) => theme.mediaWidth.upToSmall`
      width: initial;
      min-width:initial;
      max-width: initial;
  `};
`
interface NetworkContract {
  name: string
  icon: string
  chainId: ChainId
}

const networks: { [chainId in ChainId]?: NetworkContract } = {
  [ChainId.MAINNET]: {
    name: 'Ethereum',
    icon: EthIcon,
    chainId: ChainId.MAINNET
  },
  [ChainId.TELOS]: {
    name: 'Telos EVM',
    icon: TelosIcon,
    chainId: ChainId.TELOS
  },
  [ChainId.BSC]: {
    name: 'BSC',
    icon: BscIcon,
    chainId: ChainId.BSC
  },
  [ChainId.OKEX]: {
    name: 'OKEx',
    icon: OkexIcon,
    chainId: ChainId.OKEX
  },
  [ChainId.XDAI]: {
    name: 'Gnosys',
    icon: GnosysIcon,
    chainId: ChainId.XDAI
  },
  [ChainId.FUSE]: {
    name: 'Fuse',
    icon: FuseIcon,
    chainId: ChainId.FUSE
  },
  [ChainId.HECO]: {
    name: 'Heco',
    icon: HecoIcon,
    chainId: ChainId.HECO
  },
  [ChainId.MATIC]: {
    name: 'Polygon',
    icon: PolygonIcon,
    chainId: ChainId.MATIC
  },
  [ChainId.FANTOM]: {
    name: 'Fantom',
    icon: FantomIcon,
    chainId: ChainId.FANTOM
  },
  [ChainId.MOONRIVER]: {
    name: 'Moonriver',
    icon: MoonriverIcon,
    chainId: ChainId.MOONRIVER
  },
  [ChainId.ARBITRUM]: {
    name: 'Arbitrum',
    icon: ArbitrumIcon,
    chainId: ChainId.ARBITRUM
  },
  [ChainId.AVALANCHE]: {
    name: 'Avalanche',
    icon: AvalanceIcon,
    chainId: ChainId.AVALANCHE
  }
  /*[ChainId.HARMONY]: {
    name: 'Harmony',
    icon: HarmonyIcon,
    chainId: ChainId.HARMONY
  }*/
}

export const SUPPORTED_NETWORKS: {
  [chainId in ChainId]?: {
    chainId: string
    chainName: string
    nativeCurrency: {
      name: string
      symbol: string
      decimals: number
    }
    rpcUrls: string[]
    blockExplorerUrls: string[]
  }
} = {
  [ChainId.MAINNET]: {
    chainId: '0x1',
    chainName: 'Ethereum',
    nativeCurrency: {
      name: 'Ethereum',
      symbol: 'ETH',
      decimals: 18
    },
    rpcUrls: ['https://mainnet.infura.io/v3'],
    blockExplorerUrls: ['https://etherscan.com']
  },
  [ChainId.FANTOM]: {
    chainId: '0xfa',
    chainName: 'Fantom',
    nativeCurrency: {
      name: 'Fantom',
      symbol: 'FTM',
      decimals: 18
    },
    rpcUrls: ['https://rpcapi.fantom.network'],
    blockExplorerUrls: ['https://ftmscan.com']
  },
  [ChainId.BSC]: {
    chainId: '0x38',
    chainName: 'Binance Smart Chain',
    nativeCurrency: {
      name: 'Binance Coin',
      symbol: 'BNB',
      decimals: 18
    },
    rpcUrls: ['https://bsc-dataseed.binance.org'],
    blockExplorerUrls: ['https://bscscan.com']
  },
  [ChainId.MATIC]: {
    chainId: '0x89',
    chainName: 'Matic',
    nativeCurrency: {
      name: 'Matic',
      symbol: 'MATIC',
      decimals: 18
    },
    rpcUrls: ['https://polygon-rpc.com'], // ['https://matic-mainnet.chainstacklabs.com/'],
    blockExplorerUrls: ['https://polygonscan.com']
  },
  [ChainId.HECO]: {
    chainId: '0x80',
    chainName: 'Heco',
    nativeCurrency: {
      name: 'Heco Token',
      symbol: 'HT',
      decimals: 18
    },
    rpcUrls: ['https://http-mainnet.hecochain.com'],
    blockExplorerUrls: ['https://hecoinfo.com']
  },
  [ChainId.XDAI]: {
    chainId: '0x64',
    chainName: 'xDai',
    nativeCurrency: {
      name: 'xDai Token',
      symbol: 'xDai',
      decimals: 18
    },
    rpcUrls: ['https://rpc.xdaichain.com'],
    blockExplorerUrls: ['https://blockscout.com/poa/xdai']
  },
  [ChainId.HARMONY]: {
    chainId: '0x63564C40',
    chainName: 'Harmony',
    nativeCurrency: {
      name: 'One Token',
      symbol: 'ONE',
      decimals: 18
    },
    rpcUrls: [
      'https://api.harmony.one',
      'https://s1.api.harmony.one',
      'https://s2.api.harmony.one',
      'https://s3.api.harmony.one'
    ],
    blockExplorerUrls: ['https://explorer.harmony.one/']
  },
  [ChainId.AVALANCHE]: {
    chainId: '0xA86A',
    chainName: 'Avalanche Mainnet C-Chain',
    nativeCurrency: {
      name: 'Avalanche Token',
      symbol: 'AVAX',
      decimals: 18
    },
    rpcUrls: ['https://api.avax.network/ext/bc/C/rpc'],
    blockExplorerUrls: ['https://snowtrace.io']
  },
  [ChainId.OKEX]: {
    chainId: '0x42',
    chainName: 'OKEx',
    nativeCurrency: {
      name: 'OKEx Token',
      symbol: 'OKT',
      decimals: 18
    },
    rpcUrls: ['https://exchainrpc.okex.org'],
    blockExplorerUrls: ['https://www.oklink.com/okexchain']
  },
  [ChainId.ARBITRUM]: {
    chainId: '0xA4B1',
    chainName: 'Arbitrum',
    nativeCurrency: {
      name: 'Ethereum',
      symbol: 'ETH',
      decimals: 18
    },
    rpcUrls: ['https://arb1.arbitrum.io/rpc'],
    blockExplorerUrls: ['https://arbiscan.io']
  },
  [ChainId.MOONRIVER]: {
    chainId: '0x505',
    chainName: 'Moonriver',
    nativeCurrency: {
      name: 'Moonriver',
      symbol: 'MOVR',
      decimals: 18
    },
    rpcUrls: ['https://rpc.api.moonriver.moonbeam.network'],
    blockExplorerUrls: ['https://moonriver.moonscan.io']
  },
  [ChainId.FUSE]: {
    chainId: '0x7A',
    chainName: 'Fuse',
    nativeCurrency: {
      name: 'Fuse',
      symbol: 'FUSE',
      decimals: 18
    },
    rpcUrls: ['https://rpc.fuse.io'],
    blockExplorerUrls: ['https://explorer.fuse.io']
  },
  [ChainId.TELOS]: {
    chainId: '0x28',
    chainName: 'Telos',
    nativeCurrency: {
      name: 'Telos',
      symbol: 'TLOS',
      decimals: 18
    },
    rpcUrls: ['https://mainnet.telos.net/evm'],
    blockExplorerUrls: ['https://rpc1.us.telos.net/v2/explore']
  }
}

export function Network() {
  const [isOpen, setIsOpen] = useState(false)
  const { chainId } = useActiveWeb3React()
  // toggle wallet when disconnected
  const realChainId = chainId || 1

  return (
    <>
      <NetworksEl faded={true} onClick={() => setIsOpen(true)}>
        <NetworkImage src={networks[realChainId] ? networks[realChainId]?.icon : networks[1]?.icon} />
      </NetworksEl>
      <NetworkModal isOpen={isOpen} onDismiss={() => setIsOpen(false)} />
    </>
  )
}

export function NetworkModal({ isOpen, onDismiss }: { isOpen: boolean; onDismiss: () => void }) {
  const { chainId, library, account } = useActiveWeb3React()
  const toggleWalletModal = useWalletModalToggle()

  return (
    <>
      <Modal isOpen={isOpen} maxHeight={80} maxWidth={520} minHeight={20} onDismiss={onDismiss}>
        <ContentWrapper>
          <PaddedColumn gap="16px">
            <RowBetween>
              <Text fontWeight={500} fontSize={16}>
                Select a network
              </Text>
              <CloseIcon onClick={onDismiss} />
            </RowBetween>
            <NetworksWrapper>
              {Object.values(networks).map((network, index) => {
                return (
                  network && (
                    <NetworkRow
                      key={index}
                      className={chainId === network.chainId ? 'active' : ''}
                      onClick={async () => {
                        if (!account) {
                          toggleWalletModal()
                          return
                        }

                        console.debug(`Switching to chain ${network.chainId}`, SUPPORTED_NETWORKS[network.chainId])
                        const params = SUPPORTED_NETWORKS[network.chainId]
                        //cookie.set('chainId', key, params)

                        try {
                          await library?.send('wallet_switchEthereumChain', [
                            { chainId: `0x${network.chainId.toString(16)}` },
                            account
                          ])
                        } catch (switchError) {
                          if (switchError.code === 4902) {
                            try {
                              await library?.send('wallet_addEthereumChain', [params, account])
                            } catch (addError) {
                              console.error(`Add chain error ${addError}`)
                            }
                          }
                          console.error(`Switch chain error ${switchError}`)
                        }
                      }}
                    >
                      <NetworkSelectionImageWrapper>
                        <NetworkSelectionImage src={network.icon} />
                      </NetworkSelectionImageWrapper>
                      <NetworkName>{network.name}</NetworkName>
                    </NetworkRow>
                  )
                )
              })}
            </NetworksWrapper>
          </PaddedColumn>
        </ContentWrapper>
      </Modal>
    </>
  )
}

export function Networks() {
  const [isOpen, setIsOpen] = useState(false)
  const { chainId } = useActiveWeb3React()
  const realChainId = chainId || 1

  return (
    <>
      {' '}
      <ButtonSecondary onClick={() => setIsOpen(true)}>
        Choose a network <Image src={networks[realChainId] ? networks[realChainId]?.icon : networks[1]?.icon} />
      </ButtonSecondary>
      <NetworkModal isOpen={isOpen} onDismiss={() => setIsOpen(false)} />
    </>
  )
}

//import { Trade } from '@uniswap/sdk'
import { Token, Trade } from '../../lib/'
import { Token as TokenUniswap, Trade as TradeUniswap } from '../../libUniswap/'
import React, { Fragment, memo, useContext } from 'react'
import { ChevronRight } from 'react-feather'
import { Flex } from 'rebass'
import { ThemeContext } from 'styled-components'
import { TYPE } from '../../theme'
import { unwrappedToken } from 'utils/wrappedCurrency'
import { useCurrencySymbol } from '../../hooks/Tokens'

// eslint-disable-next-line react/prop-types
export default memo(function SwapRoute({ trade }: { trade: Trade | TradeUniswap }) {
  const theme = useContext(ThemeContext)

  if (trade instanceof Trade) {
    return (
      <Flex flexWrap="wrap" width="100%" justifyContent="flex-end" alignItems="center">
        {/* eslint-disable-next-line react/prop-types */}
        {trade.route.path.map((token: Token, i: string | number | null | undefined, path: string | any[]) => {
          const isLastItem: boolean = i === path.length - 1
          const currency = unwrappedToken(token)

          // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
          // @ts-ignore
          // eslint-disable-next-line react-hooks/rules-of-hooks
          const currencySymbol = useCurrencySymbol(currency)

          return (
            <Fragment key={i}>
              <Flex alignItems="end">
                <TYPE.black fontSize={14} color={theme.text1} ml="0.125rem" mr="0.125rem">
                  {currencySymbol}
                </TYPE.black>
              </Flex>
              {isLastItem ? null : <ChevronRight size={12} color={theme.text2} />}
            </Fragment>
          )
        })}
      </Flex>
    )
  } else {
    return (
      <Flex flexWrap="wrap" width="100%" justifyContent="flex-end" alignItems="center">
        {/* eslint-disable-next-line react/prop-types */}
        {trade.route.path.map((token: TokenUniswap, i: string | number | null | undefined, path: string | any[]) => {
          const isLastItem: boolean = i === path.length - 1
          const currency = unwrappedToken(token)

          // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
          // @ts-ignore
          // eslint-disable-next-line react-hooks/rules-of-hooks
          const currencySymbol = useCurrencySymbol(currency)

          return (
            <Fragment key={i}>
              <Flex alignItems="end">
                <TYPE.black fontSize={14} color={theme.text1} ml="0.125rem" mr="0.125rem">
                  {currencySymbol}
                </TYPE.black>
              </Flex>
              {isLastItem ? null : <ChevronRight size={12} color={theme.text2} />}
            </Fragment>
          )
        })}
      </Flex>
    )
  }
})

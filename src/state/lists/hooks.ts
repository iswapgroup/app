import { UNSUPPORTED_LIST_URLS } from './../../constants/lists'
import DEFAULT_TOKEN_LIST_UNI_ETH from '@uniswap/default-token-list'
import DEFAULT_TOKEN_LIST_BSC from '../../constants/tokenLists/pancakeswap-top-100.json'
import DEFAULT_TOKEN_LIST_MATIC from '../../constants/tokenLists/quickswap-default.tokenlist.json'
// import DEFAULT_TOKEN_LIST_MOONRIVER from '../../constants/tokenLists/solarbeam.tokenlist.json'
//import DEFAULT_TOKEN_LIST_ARBITRUM from '../../constants/tokenLists/token-list-42161.json'
//import DEFAULT_TOKEN_LIST_AVALANCHE from '../../constants/tokenLists/ab.tokenlist.json'
import DEFAULT_TOKEN_LIST_SUSHISWAP from '../../constants/tokenLists/sushiswap-default.tokenlist.json'
//import { ChainId, Token } from '@uniswap/sdk'
import { ChainId, Token } from '../../lib/'
import { Tags, TokenInfo, TokenList } from '@uniswap/token-lists'
import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { AppState } from '../index'
import sortByListPriority from 'utils/listSort'
import UNSUPPORTED_TOKEN_LIST from '../../constants/tokenLists/uniswap-v2-unsupported.tokenlist.json'
import { useActiveWeb3React } from '../../hooks'
//import { useActiveWeb3React } from '../../hooks'

type TagDetails = Tags[keyof Tags]
export interface TagInfo extends TagDetails {
  id: string
}

/**
 * Token instances created from token info.
 */
export class WrappedTokenInfo extends Token {
  public readonly tokenInfo: TokenInfo
  public readonly tags: TagInfo[]
  constructor(tokenInfo: TokenInfo, tags: TagInfo[]) {
    super(tokenInfo.chainId, tokenInfo.address, tokenInfo.decimals, tokenInfo.symbol, tokenInfo.name)
    this.tokenInfo = tokenInfo
    this.tags = tags
  }
  public get logoURI(): string | undefined {
    return this.tokenInfo.logoURI
  }
}

export type TokenAddressMap = Readonly<
  { [chainId in ChainId]: Readonly<{ [tokenAddress: string]: { token: WrappedTokenInfo; list: TokenList } }> }
>

/**
 * An empty result, useful as a default.
 */
const EMPTY_LIST: TokenAddressMap = {
  [ChainId.KOVAN]: {},
  [ChainId.RINKEBY]: {},
  [ChainId.ROPSTEN]: {},
  [ChainId.GÖRLI]: {},
  [ChainId.TELOS]: {},
  [ChainId.MAINNET]: {},
  [ChainId.BSC]: {},
  [ChainId.OKEX]: {},
  [ChainId.XDAI]: {},
  [ChainId.FUSE]: {},
  [ChainId.HECO]: {},
  [ChainId.MATIC]: {},
  [ChainId.FANTOM]: {},
  [ChainId.MOONRIVER]: {},
  [ChainId.ARBITRUM]: {},
  [ChainId.AVALANCHE]: {},
  [ChainId.HARMONY]: {}
}

const TOKEN_LIST = {
  [ChainId.MAINNET]: DEFAULT_TOKEN_LIST_UNI_ETH,
  [ChainId.TELOS]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.BSC]: DEFAULT_TOKEN_LIST_BSC,
  [ChainId.OKEX]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.XDAI]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.FUSE]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.HECO]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.MATIC]: DEFAULT_TOKEN_LIST_MATIC,
  [ChainId.FANTOM]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.MOONRIVER]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.ARBITRUM]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.AVALANCHE]: DEFAULT_TOKEN_LIST_SUSHISWAP,
  [ChainId.HARMONY]: DEFAULT_TOKEN_LIST_SUSHISWAP
}

//const listCache: WeakMap<TokenList, TokenAddressMap> | null =
//  typeof WeakMap !== 'undefined' ? new WeakMap<TokenList, TokenAddressMap>() : null

export function listToTokenMap(list: TokenList, chainId: number | undefined): TokenAddressMap {
  // const result = listCache?.get(list)
  //if (result) return result

  const map = list.tokens
    .filter(token => token.chainId === chainId)
    .reduce<TokenAddressMap>(
      (tokenMap, tokenInfo) => {
        const tags: TagInfo[] =
          tokenInfo.tags
            ?.map(tagId => {
              if (!list.tags?.[tagId]) return undefined
              return { ...list.tags[tagId], id: tagId }
            })
            ?.filter((x): x is TagInfo => Boolean(x)) ?? []

        const token = new WrappedTokenInfo(tokenInfo, tags)

        if (tokenMap[token.chainId][token.address] !== undefined) {
          //console.error(new Error(`Duplicate token! ${token.address}`))
          return tokenMap
        }
        return {
          ...tokenMap,
          [token.chainId]: {
            ...tokenMap[token.chainId],
            [token.address]: {
              token,
              list: list
            }
          }
        }
      },
      { ...EMPTY_LIST }
    )
  //listCache?.set(list, map)
  return map
}

export function useAllLists(): {
  readonly [url: string]: {
    readonly current: TokenList | null
    readonly pendingUpdate: TokenList | null
    readonly loadingRequestId: string | null
    readonly error: string | null
  }
} {
  return useSelector<AppState, AppState['lists']['byUrl']>(state => {
    return state.lists.byUrl
  })
}

function combineMaps(map1: TokenAddressMap, map2: TokenAddressMap): TokenAddressMap {
  return {
    1: { ...map1[1], ...map2[1] },
    3: { ...map1[3], ...map2[3] },
    4: { ...map1[4], ...map2[4] },
    5: { ...map1[5], ...map2[5] },
    40: { ...map1[40], ...map2[40] },
    42: { ...map1[42], ...map2[42] },
    56: { ...map1[56], ...map2[56] },
    66: { ...map1[66], ...map2[66] },
    100: { ...map1[100], ...map2[100] },
    122: { ...map1[122], ...map2[122] },
    128: { ...map1[128], ...map2[128] },
    137: { ...map1[137], ...map2[137] },
    250: { ...map1[250], ...map2[250] },
    1285: { ...map1[1285], ...map2[1285] },
    42161: { ...map1[42161], ...map2[42161] },
    43114: { ...map1[43114], ...map2[43114] },
    1666600000: { ...map1[1666600000], ...map2[1666600000] }
  }
}

// merge tokens contained within lists from urls
function useCombinedTokenMapFromUrls(urls: string[] | undefined): TokenAddressMap {
  const lists = useAllLists()
  const { chainId } = useActiveWeb3React()

  return useMemo(() => {
    if (!urls) return EMPTY_LIST
    return (
      urls
        .slice()
        // sort by priority so top priority goes last
        .sort(sortByListPriority)
        .reduce((allTokens, currentUrl) => {
          const current = lists[currentUrl]?.current
          if (!current) return allTokens
          try {
            const newTokens = Object.assign(listToTokenMap(current, chainId))
            return combineMaps(allTokens, newTokens)
          } catch (error) {
            console.error('Could not show token list due to error', error)
            return allTokens
          }
        }, EMPTY_LIST)
    )
  }, [lists, urls])
}

// filter out unsupported lists
export function useActiveListUrls(): string[] | undefined {
  return useSelector<AppState, AppState['lists']['activeListUrls']>(state => state.lists.activeListUrls)?.filter(
    url => !UNSUPPORTED_LIST_URLS.includes(url)
  )
}

export function useInactiveListUrls(): string[] {
  const lists = useAllLists()
  const allActiveListUrls = useActiveListUrls()
  return Object.keys(lists).filter(url => !allActiveListUrls?.includes(url) && !UNSUPPORTED_LIST_URLS.includes(url))
}

export function getDefaultTokenList(): TokenList {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { chainId } = useActiveWeb3React()

  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  return TOKEN_LIST[chainId || 1]
}

// get all the tokens from active lists, combine with local default tokens
export function useCombinedActiveList(): TokenAddressMap {
  const activeListUrls = useActiveListUrls()
  const { chainId } = useActiveWeb3React()
  const activeTokens = useCombinedTokenMapFromUrls(activeListUrls)
  const defaultTokenMap = listToTokenMap(getDefaultTokenList(), chainId)
  return combineMaps(activeTokens, defaultTokenMap)
}

// all tokens from inactive lists
export function useCombinedInactiveList(): TokenAddressMap {
  const allInactiveListUrls: string[] = useInactiveListUrls()
  return useCombinedTokenMapFromUrls(allInactiveListUrls)
}

// used to hide warnings on import for default tokens
export function useDefaultTokenList(): TokenAddressMap {
  const { chainId } = useActiveWeb3React()
  return listToTokenMap(getDefaultTokenList(), chainId)
}

// list of tokens not supported on interface, used to show warnings and prevent swaps and adds
export function useUnsupportedTokenList(): TokenAddressMap {
  const { chainId } = useActiveWeb3React()
  // get hard coded unsupported tokens
  const localUnsupportedListMap = listToTokenMap(UNSUPPORTED_TOKEN_LIST, chainId)

  // get any loaded unsupported tokens
  const loadedUnsupportedListMap = useCombinedTokenMapFromUrls(UNSUPPORTED_LIST_URLS)

  // format into one token address map
  return combineMaps(localUnsupportedListMap, loadedUnsupportedListMap)
}

export function useIsListActive(url: string): boolean {
  const activeListUrls = useActiveListUrls()
  return Boolean(activeListUrls?.includes(url))
}

import { ZERO_PERCENT, ONE_HUNDRED_PERCENT } from './../constants/index'
//import { Trade, Percent, currencyEquals } from '@uniswap/sdk'
import { Trade, Percent, currencyEquals } from '../lib/'
import { Trade as TradeUniswap } from '../libUniswap/'

// returns whether tradeB is better than tradeA by at least a threshold percentage amount

export function isTradeBetter(
  tradeA: Trade | TradeUniswap | undefined | null,
  tradeB: Trade | TradeUniswap | undefined | null,
  minimumDelta: Percent = ZERO_PERCENT
): boolean | undefined {
  if (tradeA && !tradeB) return false
  if (tradeB && !tradeA) return true
  if (!tradeA || !tradeB) return undefined

  if (
    tradeA.tradeType !== tradeB.tradeType ||
    !currencyEquals(tradeA.inputAmount.currency, tradeB.inputAmount.currency) ||
    !currencyEquals(tradeB.outputAmount.currency, tradeB.outputAmount.currency)
  ) {
    throw new Error('Trades are not comparable')
  }

  if (minimumDelta.equalTo(ZERO_PERCENT)) {
    return tradeA.executionPrice.lessThan(tradeB.executionPrice)
  } else {
    return tradeA.executionPrice.raw.multiply(minimumDelta.add(ONE_HUNDRED_PERCENT)).lessThan(tradeB.executionPrice)
  }
}

//import { Currency, ETHER, Token } from '@uniswap/sdk'
import { Currency, ETHER, Token } from '../lib/'

export function currencyId(currency: Currency): string {
  if (currency === ETHER) return 'ETH'
  if (currency instanceof Token) return currency.address
  throw new Error('invalid currency')
}

import { Web3Provider } from '@ethersproject/providers'
import { InjectedConnector } from '@web3-react/injected-connector'
import { WalletConnectConnector } from '@web3-react/walletconnect-connector'
import { WalletLinkConnector } from '@web3-react/walletlink-connector'
import { PortisConnector } from '@web3-react/portis-connector'

import { FortmaticConnector } from './Fortmatic'
import { NetworkConnector } from './NetworkConnector'

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_ETHEREUM = process.env.REACT_APP_NETWORK_URL_ETHEREUM
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_TELOS = process.env.REACT_APP_NETWORK_URL_TELOS
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_BSC = process.env.REACT_APP_NETWORK_URL_BSC
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_OKEX = process.env.REACT_APP_NETWORK_URL_OKEX
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_XDAI = process.env.REACT_APP_NETWORK_URL_XDAI
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_FUSE = process.env.REACT_APP_NETWORK_URL_FUSE
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_HECO = process.env.REACT_APP_NETWORK_URL_HECO
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_MATIC = process.env.REACT_APP_NETWORK_URL_MATIC
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_FANTOM = process.env.REACT_APP_NETWORK_URL_FANTOM
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_MOONRIVER = process.env.REACT_APP_NETWORK_URL_MOONRIVER
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_ARBITRUM = process.env.REACT_APP_NETWORK_URL_ARBITRUM
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_AVALANCHE = process.env.REACT_APP_NETWORK_URL_AVALANCHE
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const NETWORK_URL_HARMONY = process.env.REACT_APP_NETWORK_URL_HARMONY

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const FORMATIC_KEY = process.env.REACT_APP_FORTMATIC_KEY
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const PORTIS_ID = process.env.REACT_APP_PORTIS_ID
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
const WALLETCONNECT_BRIDGE_URL = process.env.REACT_APP_WALLETCONNECT_BRIDGE_URL

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
export const NETWORK_CHAIN_ID: number = parseInt(process.env.REACT_APP_CHAIN_ID ?? '1')

if (typeof NETWORK_URL_ETHEREUM === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_TELOS === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_BSC === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_OKEX === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_MATIC === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_FANTOM === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_MOONRIVER === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_ARBITRUM === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_AVALANCHE === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_XDAI === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_FUSE === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_HECO === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

if (typeof NETWORK_URL_HARMONY === 'undefined') {
  throw new Error(`REACT_APP_NETWORK_URL must be a defined environment variable`)
}

export const network = new NetworkConnector({
  urls: {
    [1]: NETWORK_URL_ETHEREUM,
    [40]: NETWORK_URL_TELOS,
    [56]: NETWORK_URL_BSC,
    [66]: NETWORK_URL_OKEX,
    [100]: NETWORK_URL_XDAI,
    [122]: NETWORK_URL_FUSE,
    [128]: NETWORK_URL_HECO,
    [137]: NETWORK_URL_MATIC,
    [250]: NETWORK_URL_FANTOM,
    [1285]: NETWORK_URL_MOONRIVER,
    [42161]: NETWORK_URL_ARBITRUM,
    [43114]: NETWORK_URL_AVALANCHE
    //[1666600000]: NETWORK_URL_HARMONY
  },
  defaultChainId: 1
})

let networkLibrary: Web3Provider | undefined
export function getNetworkLibrary(): Web3Provider {
  return (networkLibrary = networkLibrary ?? new Web3Provider(network.provider as any))
}

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
export const injected = new InjectedConnector({
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  // @ts-nocheck
  supportedChainIds: [1, 40, 56, 66, 100, 122, 128, 137, 250, 1285, 42161, 43114] //, 1666600000]
})

// mainnet only
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
export const walletconnect = new WalletConnectConnector({
  rpc: {
    1: NETWORK_URL_ETHEREUM,
    40: NETWORK_URL_TELOS,
    56: NETWORK_URL_BSC,
    66: NETWORK_URL_OKEX,
    100: NETWORK_URL_XDAI,
    122: NETWORK_URL_FUSE,
    128: NETWORK_URL_HECO,
    137: NETWORK_URL_MATIC,
    250: NETWORK_URL_FANTOM,
    1285: NETWORK_URL_MOONRIVER,
    42161: NETWORK_URL_ARBITRUM,
    43114: NETWORK_URL_AVALANCHE
    //1666600000: NETWORK_URL_HARMONY
  },
  bridge: WALLETCONNECT_BRIDGE_URL,
  qrcode: true,
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  // @ts-nocheck
  pollingInterval: 15000
})

// mainnet only
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
// @ts-nocheck
export const fortmatic = new FortmaticConnector({
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  // @ts-nocheck
  apiKey: FORMATIC_KEY ?? '',
  chainId: 1
})

// mainnet only
export const portis = new PortisConnector({
  dAppId: PORTIS_ID ?? '',
  networks: [1]
})

// mainnet only
export const walletlink = new WalletLinkConnector({
  url: NETWORK_URL_ETHEREUM,
  appName: 'Uniswap',
  appLogoUrl: ''
})
